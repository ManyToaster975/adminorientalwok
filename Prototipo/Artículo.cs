﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public partial class Articulo : UserControl
    {
        int articulo;
        int costo;
        Ordenar ordenar;
        public static String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        public Articulo(int art, Ordenar ordenar)
        {
            InitializeComponent();
            articulo = art;
            this.ordenar = ordenar;
        }

        private void Artículo_Load(object sender, EventArgs e)
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Articulo WHERE IdArticulo=" + articulo.ToString() + "";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                lblArt.Text = dt.Rows[0]["Nombre"].ToString();
                txtDes.Text = dt.Rows[0]["Descripcion"].ToString();
                pb.ImageLocation = Application.StartupPath + "\\Imagenes\\" + articulo.ToString() + ".jpg";
                btn_add.Text=btn_add.Text+" por $"+ dt.Rows[0]["Costo"].ToString();
                costo= int.Parse(dt.Rows[0]["Costo"].ToString());
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            int cantidad = Cantidad.Show(lblArt.Text);
            if (cantidad != 0)
            {
                OrdenGlobal.ordenGlobal[OrdenGlobal.currentPerson].addArticulo(articulo, cantidad, lblArt.Text, costo);
                MessageBox.Show("Añadido a carrito");
            }
            ordenar.cargarCarrito();
        }
    }
}