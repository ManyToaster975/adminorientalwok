﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Prototipo
{
    public partial class Pedido : UserControl
    {

        private int idOrden;
        public static string cnn = ("Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + @"\BD.mdb");
        public Pedido(int idOrden)
        {
            this.InitializeComponent();
            this.idOrden = idOrden;
        }

        public void cargarOrden()
        {
            DataTable dataTable = new DataTable();
            new OleDbDataAdapter("SELECT * FROM Orden WHERE idOrden=" + this.idOrden.ToString(), cnn).Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                this.lblFecha.Text = dataTable.Rows[0]["Fecha"].ToString();
                this.lblHora.Text = dataTable.Rows[0]["Hora"].ToString();
                this.lblOrden.Text = dataTable.Rows[0]["IdOrden"].ToString();
                this.lblMonto.Text = "$ "+dataTable.Rows[0]["MontoTotal"].ToString();
                if (dataTable.Rows[0]["Estado"].ToString() == "0")
                {
                    this.lblEstatus.Text = "Activo";
                }
                if (dataTable.Rows[0]["Estado"].ToString() == "1")
                {
                    this.lblEstatus.Text = "Servida";
                }
                if (dataTable.Rows[0]["Estado"].ToString() == "2")
                {
                    this.lblEstatus.Text = "Cancelada";
                }
            }
        }

        private void Pedido_Load(object sender, EventArgs e)
        {
            cargarOrden();
        }

        private void btnDetalle_Click(object sender, EventArgs e)
        {
            new Detalles(this.idOrden, this).ShowDialog();
        }
    }
}
