﻿namespace Prototipo
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pan_Marca = new System.Windows.Forms.Panel();
            this.btn_Ord = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_Cont = new System.Windows.Forms.Button();
            this.btn_Sucursales = new System.Windows.Forms.Button();
            this.btn_Menu = new System.Windows.Forms.Button();
            this.btn_Home = new System.Windows.Forms.Button();
            this.pan_head = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Cerrar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.labelBebidas = new System.Windows.Forms.LinkLabel();
            this.labelPrincipios = new System.Windows.Forms.LinkLabel();
            this.labelPFuerte = new System.Windows.Forms.LinkLabel();
            this.labelPostres = new System.Windows.Forms.LinkLabel();
            this.panelBebidas = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.richTextBox25 = new System.Windows.Forms.RichTextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.panel29 = new System.Windows.Forms.Panel();
            this.richTextBox26 = new System.Windows.Forms.RichTextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.richTextBox27 = new System.Windows.Forms.RichTextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.panel31 = new System.Windows.Forms.Panel();
            this.richTextBox28 = new System.Windows.Forms.RichTextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.panel32 = new System.Windows.Forms.Panel();
            this.richTextBox29 = new System.Windows.Forms.RichTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.panelPFuerte = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.richTextBox6 = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.richTextBox5 = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.richTextBox4 = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.richTextBox3 = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panelPrincipios = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.richTextBox14 = new System.Windows.Forms.RichTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.richTextBox13 = new System.Windows.Forms.RichTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.richTextBox12 = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.richTextBox11 = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.richTextBox10 = new System.Windows.Forms.RichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.richTextBox9 = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.richTextBox8 = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.richTextBox7 = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.panelPostres = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.panelSucursales = new System.Windows.Forms.Panel();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.richTextBox21 = new System.Windows.Forms.RichTextBox();
            this.richTextBox22 = new System.Windows.Forms.RichTextBox();
            this.richTextBox19 = new System.Windows.Forms.RichTextBox();
            this.richTextBox20 = new System.Windows.Forms.RichTextBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.richTextBox17 = new System.Windows.Forms.RichTextBox();
            this.richTextBox18 = new System.Windows.Forms.RichTextBox();
            this.richTextBox15 = new System.Windows.Forms.RichTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.richTextBox16 = new System.Windows.Forms.RichTextBox();
            this.panelInicio = new System.Windows.Forms.Panel();
            this.panelRegistro = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.txtRegCorreo = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtRegConPass = new System.Windows.Forms.TextBox();
            this.btnRegistro = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtRegPass = new System.Windows.Forms.TextBox();
            this.txtRegUser = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.panelOrdena = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.btn_login = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panelContacto = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.richTextBox23 = new System.Windows.Forms.RichTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panelMenu.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panelBebidas.SuspendLayout();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            this.panel31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            this.panel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            this.panelPFuerte.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panelPrincipios.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.panelPostres.SuspendLayout();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            this.panelSucursales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            this.panelInicio.SuspendLayout();
            this.panelRegistro.SuspendLayout();
            this.panelOrdena.SuspendLayout();
            this.panelContacto.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel1.Controls.Add(this.pan_Marca);
            this.panel1.Controls.Add(this.btn_Ord);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.btn_Cont);
            this.panel1.Controls.Add(this.btn_Sucursales);
            this.panel1.Controls.Add(this.btn_Menu);
            this.panel1.Controls.Add(this.btn_Home);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(179, 566);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // pan_Marca
            // 
            this.pan_Marca.BackColor = System.Drawing.Color.Red;
            this.pan_Marca.Location = new System.Drawing.Point(2, 203);
            this.pan_Marca.Name = "pan_Marca";
            this.pan_Marca.Size = new System.Drawing.Size(10, 53);
            this.pan_Marca.TabIndex = 1;
            // 
            // btn_Ord
            // 
            this.btn_Ord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btn_Ord.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_Ord.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Ord.FlatAppearance.BorderSize = 0;
            this.btn_Ord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Ord.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.btn_Ord.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btn_Ord.Image = global::Prototipo.Properties.Resources.login2;
            this.btn_Ord.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Ord.Location = new System.Drawing.Point(2, 343);
            this.btn_Ord.Name = "btn_Ord";
            this.btn_Ord.Size = new System.Drawing.Size(196, 42);
            this.btn_Ord.TabIndex = 6;
            this.btn_Ord.Text = "    Inicia Sesión";
            this.btn_Ord.UseVisualStyleBackColor = false;
            this.btn_Ord.Click += new System.EventHandler(this.btn_Ord_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(63, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(91, 143);
            this.panel2.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Prototipo.Properties.Resources.Imagen2;
            this.pictureBox2.Location = new System.Drawing.Point(4, 89);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(80, 49);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Prototipo.Properties.Resources.Imagen1;
            this.pictureBox1.Location = new System.Drawing.Point(4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 80);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btn_Cont
            // 
            this.btn_Cont.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btn_Cont.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Cont.FlatAppearance.BorderSize = 0;
            this.btn_Cont.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Cont.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.btn_Cont.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btn_Cont.Image = global::Prototipo.Properties.Resources.phone_call;
            this.btn_Cont.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Cont.Location = new System.Drawing.Point(8, 477);
            this.btn_Cont.Name = "btn_Cont";
            this.btn_Cont.Size = new System.Drawing.Size(168, 48);
            this.btn_Cont.TabIndex = 5;
            this.btn_Cont.Text = "    Contacto";
            this.btn_Cont.UseVisualStyleBackColor = false;
            this.btn_Cont.Click += new System.EventHandler(this.btn_Cont_Click);
            // 
            // btn_Sucursales
            // 
            this.btn_Sucursales.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btn_Sucursales.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Sucursales.FlatAppearance.BorderSize = 0;
            this.btn_Sucursales.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Sucursales.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.btn_Sucursales.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btn_Sucursales.Image = global::Prototipo.Properties.Resources.restaurant;
            this.btn_Sucursales.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Sucursales.Location = new System.Drawing.Point(11, 413);
            this.btn_Sucursales.Name = "btn_Sucursales";
            this.btn_Sucursales.Size = new System.Drawing.Size(168, 49);
            this.btn_Sucursales.TabIndex = 3;
            this.btn_Sucursales.Text = "    Sucursales";
            this.btn_Sucursales.UseVisualStyleBackColor = false;
            this.btn_Sucursales.Click += new System.EventHandler(this.btn_Sucursales_Click);
            // 
            // btn_Menu
            // 
            this.btn_Menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btn_Menu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Menu.FlatAppearance.BorderSize = 0;
            this.btn_Menu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Menu.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.btn_Menu.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btn_Menu.Image = global::Prototipo.Properties.Resources.menu1;
            this.btn_Menu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Menu.Location = new System.Drawing.Point(11, 273);
            this.btn_Menu.Name = "btn_Menu";
            this.btn_Menu.Size = new System.Drawing.Size(168, 43);
            this.btn_Menu.TabIndex = 2;
            this.btn_Menu.Text = "    Menú";
            this.btn_Menu.UseVisualStyleBackColor = false;
            this.btn_Menu.Click += new System.EventHandler(this.btn_Menu_Click);
            // 
            // btn_Home
            // 
            this.btn_Home.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btn_Home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Home.FlatAppearance.BorderSize = 0;
            this.btn_Home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Home.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.btn_Home.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btn_Home.Image = global::Prototipo.Properties.Resources.home;
            this.btn_Home.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Home.Location = new System.Drawing.Point(11, 203);
            this.btn_Home.Name = "btn_Home";
            this.btn_Home.Size = new System.Drawing.Size(168, 53);
            this.btn_Home.TabIndex = 1;
            this.btn_Home.Text = "    Inicio";
            this.btn_Home.UseVisualStyleBackColor = false;
            this.btn_Home.Click += new System.EventHandler(this.btn_Home_Click);
            // 
            // pan_head
            // 
            this.pan_head.BackColor = System.Drawing.Color.Red;
            this.pan_head.Dock = System.Windows.Forms.DockStyle.Top;
            this.pan_head.Location = new System.Drawing.Point(179, 0);
            this.pan_head.Margin = new System.Windows.Forms.Padding(0);
            this.pan_head.Name = "pan_head";
            this.pan_head.Size = new System.Drawing.Size(860, 10);
            this.pan_head.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.btn_Cerrar);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(181, 9);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(853, 85);
            this.panel3.TabIndex = 1;
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // btn_Cerrar
            // 
            this.btn_Cerrar.BackColor = System.Drawing.Color.Transparent;
            this.btn_Cerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Cerrar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_Cerrar.FlatAppearance.BorderSize = 0;
            this.btn_Cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Cerrar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cerrar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btn_Cerrar.Image = global::Prototipo.Properties.Resources.turn_off;
            this.btn_Cerrar.Location = new System.Drawing.Point(725, 25);
            this.btn_Cerrar.Name = "btn_Cerrar";
            this.btn_Cerrar.Size = new System.Drawing.Size(38, 34);
            this.btn_Cerrar.TabIndex = 7;
            this.btn_Cerrar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Cerrar.UseVisualStyleBackColor = false;
            this.btn_Cerrar.Click += new System.EventHandler(this.btn_Cerrar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 30F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(304, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(237, 47);
            this.label3.TabIndex = 9;
            this.label3.Text = "Bienvenido";
            // 
            // panelMenu
            // 
            this.panelMenu.AutoScroll = true;
            this.panelMenu.BackColor = System.Drawing.SystemColors.Control;
            this.panelMenu.Controls.Add(this.label2);
            this.panelMenu.Controls.Add(this.panel25);
            this.panelMenu.Controls.Add(this.panelBebidas);
            this.panelMenu.Controls.Add(this.panelPFuerte);
            this.panelMenu.Controls.Add(this.panelPrincipios);
            this.panelMenu.Controls.Add(this.panelPostres);
            this.panelMenu.Location = new System.Drawing.Point(763, 19);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(827, 452);
            this.panelMenu.TabIndex = 10;
            this.panelMenu.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(376, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 28);
            this.label2.TabIndex = 0;
            this.label2.Text = "Menu";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel25.Controls.Add(this.labelBebidas);
            this.panel25.Controls.Add(this.labelPrincipios);
            this.panel25.Controls.Add(this.labelPFuerte);
            this.panel25.Controls.Add(this.labelPostres);
            this.panel25.Location = new System.Drawing.Point(23, 37);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(801, 33);
            this.panel25.TabIndex = 5;
            // 
            // labelBebidas
            // 
            this.labelBebidas.AutoSize = true;
            this.labelBebidas.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBebidas.LinkColor = System.Drawing.Color.Tomato;
            this.labelBebidas.Location = new System.Drawing.Point(656, 4);
            this.labelBebidas.Name = "labelBebidas";
            this.labelBebidas.Size = new System.Drawing.Size(88, 23);
            this.labelBebidas.TabIndex = 5;
            this.labelBebidas.TabStop = true;
            this.labelBebidas.Text = "Bebidas";
            this.labelBebidas.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labelBebidas_LinkClicked_1);
            // 
            // labelPrincipios
            // 
            this.labelPrincipios.AutoSize = true;
            this.labelPrincipios.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrincipios.LinkColor = System.Drawing.Color.Tomato;
            this.labelPrincipios.Location = new System.Drawing.Point(9, 4);
            this.labelPrincipios.Name = "labelPrincipios";
            this.labelPrincipios.Size = new System.Drawing.Size(92, 23);
            this.labelPrincipios.TabIndex = 2;
            this.labelPrincipios.TabStop = true;
            this.labelPrincipios.Text = "Entradas";
            this.labelPrincipios.Click += new System.EventHandler(this.labelPrincipios_Click);
            // 
            // labelPFuerte
            // 
            this.labelPFuerte.AutoSize = true;
            this.labelPFuerte.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPFuerte.LinkColor = System.Drawing.Color.Tomato;
            this.labelPFuerte.Location = new System.Drawing.Point(213, 4);
            this.labelPFuerte.Name = "labelPFuerte";
            this.labelPFuerte.Size = new System.Drawing.Size(124, 23);
            this.labelPFuerte.TabIndex = 4;
            this.labelPFuerte.TabStop = true;
            this.labelPFuerte.Text = "Plato Fuerte";
            this.labelPFuerte.Click += new System.EventHandler(this.labelPFuerte_Click);
            // 
            // labelPostres
            // 
            this.labelPostres.AutoSize = true;
            this.labelPostres.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPostres.LinkColor = System.Drawing.Color.Tomato;
            this.labelPostres.Location = new System.Drawing.Point(467, 4);
            this.labelPostres.Name = "labelPostres";
            this.labelPostres.Size = new System.Drawing.Size(77, 23);
            this.labelPostres.TabIndex = 3;
            this.labelPostres.TabStop = true;
            this.labelPostres.Text = "Postres";
            this.labelPostres.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.labelPostres_LinkClicked);
            // 
            // panelBebidas
            // 
            this.panelBebidas.AutoScroll = true;
            this.panelBebidas.BackColor = System.Drawing.SystemColors.Control;
            this.panelBebidas.Controls.Add(this.panel28);
            this.panelBebidas.Controls.Add(this.panel29);
            this.panelBebidas.Controls.Add(this.panel30);
            this.panelBebidas.Controls.Add(this.panel31);
            this.panelBebidas.Controls.Add(this.panel32);
            this.panelBebidas.Location = new System.Drawing.Point(23, 74);
            this.panelBebidas.Name = "panelBebidas";
            this.panelBebidas.Size = new System.Drawing.Size(801, 375);
            this.panelBebidas.TabIndex = 8;
            this.panelBebidas.Visible = false;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.SystemColors.Control;
            this.panel28.Controls.Add(this.richTextBox25);
            this.panel28.Controls.Add(this.label42);
            this.panel28.Controls.Add(this.pictureBox28);
            this.panel28.Location = new System.Drawing.Point(5, 272);
            this.panel28.Margin = new System.Windows.Forms.Padding(0);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(359, 112);
            this.panel28.TabIndex = 5;
            // 
            // richTextBox25
            // 
            this.richTextBox25.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox25.Enabled = false;
            this.richTextBox25.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox25.Location = new System.Drawing.Point(159, 25);
            this.richTextBox25.Name = "richTextBox25";
            this.richTextBox25.Size = new System.Drawing.Size(189, 89);
            this.richTextBox25.TabIndex = 0;
            this.richTextBox25.Text = "Limonada a base de agua natural 500ml";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label42.Location = new System.Drawing.Point(155, 3);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(90, 18);
            this.label42.TabIndex = 3;
            this.label42.Text = "LIMONADA";
            // 
            // pictureBox28
            // 
            this.pictureBox28.Image = global::Prototipo.Properties.Resources.limonada;
            this.pictureBox28.Location = new System.Drawing.Point(11, -2);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(102, 108);
            this.pictureBox28.TabIndex = 2;
            this.pictureBox28.TabStop = false;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.SystemColors.Control;
            this.panel29.Controls.Add(this.richTextBox26);
            this.panel29.Controls.Add(this.label43);
            this.panel29.Controls.Add(this.pictureBox29);
            this.panel29.Location = new System.Drawing.Point(419, 143);
            this.panel29.Margin = new System.Windows.Forms.Padding(0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(357, 116);
            this.panel29.TabIndex = 4;
            // 
            // richTextBox26
            // 
            this.richTextBox26.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox26.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox26.Enabled = false;
            this.richTextBox26.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox26.Location = new System.Drawing.Point(132, 23);
            this.richTextBox26.Name = "richTextBox26";
            this.richTextBox26.Size = new System.Drawing.Size(181, 89);
            this.richTextBox26.TabIndex = 0;
            this.richTextBox26.Text = "Refresco sabor naranja marca Fanta 355ml";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(128, 1);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 18);
            this.label43.TabIndex = 3;
            this.label43.Text = "FANTA";
            // 
            // pictureBox29
            // 
            this.pictureBox29.Image = global::Prototipo.Properties.Resources.fanta;
            this.pictureBox29.Location = new System.Drawing.Point(3, 3);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(122, 108);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox29.TabIndex = 2;
            this.pictureBox29.TabStop = false;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.SystemColors.Control;
            this.panel30.Controls.Add(this.richTextBox27);
            this.panel30.Controls.Add(this.label44);
            this.panel30.Controls.Add(this.pictureBox30);
            this.panel30.Location = new System.Drawing.Point(5, 143);
            this.panel30.Margin = new System.Windows.Forms.Padding(0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(357, 116);
            this.panel30.TabIndex = 3;
            // 
            // richTextBox27
            // 
            this.richTextBox27.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox27.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox27.Enabled = false;
            this.richTextBox27.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox27.Location = new System.Drawing.Point(159, 25);
            this.richTextBox27.Name = "richTextBox27";
            this.richTextBox27.Size = new System.Drawing.Size(189, 89);
            this.richTextBox27.TabIndex = 0;
            this.richTextBox27.Text = "Refresco sabor limon marca Sprite 355ml";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(155, 3);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(51, 18);
            this.label44.TabIndex = 3;
            this.label44.Text = "SPRITE";
            // 
            // pictureBox30
            // 
            this.pictureBox30.Image = global::Prototipo.Properties.Resources.sprite;
            this.pictureBox30.Location = new System.Drawing.Point(11, 7);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(102, 96);
            this.pictureBox30.TabIndex = 2;
            this.pictureBox30.TabStop = false;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.SystemColors.Control;
            this.panel31.Controls.Add(this.richTextBox28);
            this.panel31.Controls.Add(this.label45);
            this.panel31.Controls.Add(this.pictureBox31);
            this.panel31.Location = new System.Drawing.Point(419, 9);
            this.panel31.Margin = new System.Windows.Forms.Padding(0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(356, 120);
            this.panel31.TabIndex = 2;
            // 
            // richTextBox28
            // 
            this.richTextBox28.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox28.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox28.Enabled = false;
            this.richTextBox28.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox28.Location = new System.Drawing.Point(132, 31);
            this.richTextBox28.Name = "richTextBox28";
            this.richTextBox28.Size = new System.Drawing.Size(181, 74);
            this.richTextBox28.TabIndex = 0;
            this.richTextBox28.Text = "Refresco de cola regular 355ml";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label45.Location = new System.Drawing.Point(131, 10);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(105, 18);
            this.label45.TabIndex = 3;
            this.label45.Text = "COCA-COLA";
            // 
            // pictureBox31
            // 
            this.pictureBox31.Image = global::Prototipo.Properties.Resources.coca;
            this.pictureBox31.Location = new System.Drawing.Point(3, 3);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(107, 112);
            this.pictureBox31.TabIndex = 2;
            this.pictureBox31.TabStop = false;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.SystemColors.Control;
            this.panel32.Controls.Add(this.richTextBox29);
            this.panel32.Controls.Add(this.label46);
            this.panel32.Controls.Add(this.pictureBox32);
            this.panel32.Location = new System.Drawing.Point(5, 13);
            this.panel32.Margin = new System.Windows.Forms.Padding(0);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(357, 116);
            this.panel32.TabIndex = 1;
            // 
            // richTextBox29
            // 
            this.richTextBox29.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox29.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox29.Enabled = false;
            this.richTextBox29.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox29.Location = new System.Drawing.Point(159, 25);
            this.richTextBox29.Name = "richTextBox29";
            this.richTextBox29.Size = new System.Drawing.Size(189, 89);
            this.richTextBox29.TabIndex = 0;
            this.richTextBox29.Text = "Agua purificada en botella";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label46.Location = new System.Drawing.Point(155, 3);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(162, 18);
            this.label46.TabIndex = 3;
            this.label46.Text = "AGUA EMBOTELLADA";
            // 
            // pictureBox32
            // 
            this.pictureBox32.Image = global::Prototipo.Properties.Resources.agua;
            this.pictureBox32.Location = new System.Drawing.Point(29, 5);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(76, 108);
            this.pictureBox32.TabIndex = 2;
            this.pictureBox32.TabStop = false;
            // 
            // panelPFuerte
            // 
            this.panelPFuerte.AutoScroll = true;
            this.panelPFuerte.BackColor = System.Drawing.SystemColors.Control;
            this.panelPFuerte.Controls.Add(this.panel9);
            this.panelPFuerte.Controls.Add(this.panel8);
            this.panelPFuerte.Controls.Add(this.panel7);
            this.panelPFuerte.Controls.Add(this.panel6);
            this.panelPFuerte.Controls.Add(this.panel5);
            this.panelPFuerte.Controls.Add(this.panel4);
            this.panelPFuerte.Location = new System.Drawing.Point(23, 74);
            this.panelPFuerte.Name = "panelPFuerte";
            this.panelPFuerte.Size = new System.Drawing.Size(804, 365);
            this.panelPFuerte.TabIndex = 2;
            this.panelPFuerte.Visible = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.Control;
            this.panel9.Controls.Add(this.richTextBox6);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.pictureBox8);
            this.panel9.Location = new System.Drawing.Point(419, 272);
            this.panel9.Margin = new System.Windows.Forms.Padding(0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(356, 112);
            this.panel9.TabIndex = 6;
            // 
            // richTextBox6
            // 
            this.richTextBox6.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox6.Enabled = false;
            this.richTextBox6.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox6.Location = new System.Drawing.Point(131, 25);
            this.richTextBox6.Name = "richTextBox6";
            this.richTextBox6.Size = new System.Drawing.Size(219, 84);
            this.richTextBox6.TabIndex = 0;
            this.richTextBox6.Text = "Platillo de pollo con vegetales crujientes (zanahorias, champiñones, ejote, cebol" +
    "la y germinado de soya) mezclado con picante.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(129, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(190, 18);
            this.label11.TabIndex = 3;
            this.label11.Text = "GENERAL SPICY CHICKEN";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Prototipo.Properties.Resources.general_spicy;
            this.pictureBox8.Location = new System.Drawing.Point(3, 3);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(122, 108);
            this.pictureBox8.TabIndex = 2;
            this.pictureBox8.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.Controls.Add(this.richTextBox5);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.pictureBox7);
            this.panel8.Location = new System.Drawing.Point(5, 272);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(359, 112);
            this.panel8.TabIndex = 5;
            // 
            // richTextBox5
            // 
            this.richTextBox5.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox5.Enabled = false;
            this.richTextBox5.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox5.Location = new System.Drawing.Point(159, 25);
            this.richTextBox5.Name = "richTextBox5";
            this.richTextBox5.Size = new System.Drawing.Size(189, 89);
            this.richTextBox5.TabIndex = 0;
            this.richTextBox5.Text = "Pollo en salsa agridulce con zanahoria, pimiento verde, cebolla y piña.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(155, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(145, 18);
            this.label10.TabIndex = 3;
            this.label10.Text = "POLLO AGRIDULCE";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Prototipo.Properties.Resources.pollo_agridulce;
            this.pictureBox7.Location = new System.Drawing.Point(3, 3);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(150, 108);
            this.pictureBox7.TabIndex = 2;
            this.pictureBox7.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this.richTextBox4);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.pictureBox6);
            this.panel7.Location = new System.Drawing.Point(419, 143);
            this.panel7.Margin = new System.Windows.Forms.Padding(0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(357, 116);
            this.panel7.TabIndex = 4;
            // 
            // richTextBox4
            // 
            this.richTextBox4.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox4.Enabled = false;
            this.richTextBox4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox4.Location = new System.Drawing.Point(132, 23);
            this.richTextBox4.Name = "richTextBox4";
            this.richTextBox4.Size = new System.Drawing.Size(181, 89);
            this.richTextBox4.TabIndex = 0;
            this.richTextBox4.Text = "Pollo en salsa hunan con cebolla cambray y ajonjolí.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(128, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 18);
            this.label9.TabIndex = 3;
            this.label9.Text = "SPICY HUNAN";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Prototipo.Properties.Resources._9;
            this.pictureBox6.Location = new System.Drawing.Point(3, 3);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(122, 108);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 2;
            this.pictureBox6.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.richTextBox3);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Controls.Add(this.pictureBox5);
            this.panel6.Location = new System.Drawing.Point(5, 143);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(357, 116);
            this.panel6.TabIndex = 3;
            // 
            // richTextBox3
            // 
            this.richTextBox3.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox3.Enabled = false;
            this.richTextBox3.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox3.Location = new System.Drawing.Point(159, 25);
            this.richTextBox3.Name = "richTextBox3";
            this.richTextBox3.Size = new System.Drawing.Size(189, 89);
            this.richTextBox3.TabIndex = 0;
            this.richTextBox3.Text = "Pollo en salsa Kung Pao con brócoli, cacahuate y cebolla cambray.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(155, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(157, 18);
            this.label8.TabIndex = 3;
            this.label8.Text = "KUNG PAO CHICKEN";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Prototipo.Properties.Resources.kung_pao;
            this.pictureBox5.Location = new System.Drawing.Point(3, 3);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(150, 108);
            this.pictureBox5.TabIndex = 2;
            this.pictureBox5.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.Controls.Add(this.richTextBox2);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.pictureBox4);
            this.panel5.Location = new System.Drawing.Point(419, 9);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(356, 120);
            this.panel5.TabIndex = 2;
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Enabled = false;
            this.richTextBox2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.Location = new System.Drawing.Point(132, 31);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(181, 74);
            this.richTextBox2.TabIndex = 0;
            this.richTextBox2.Text = "Pollo en salsa especial a base de soya con jícama, zanahoria, pimiento verde y br" +
    "ócoli.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(131, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 18);
            this.label7.TabIndex = 3;
            this.label7.Text = "POLLO A LA CHINA";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Prototipo.Properties.Resources.pollo_chian;
            this.pictureBox4.Location = new System.Drawing.Point(3, 3);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(122, 112);
            this.pictureBox4.TabIndex = 2;
            this.pictureBox4.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.richTextBox1);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Location = new System.Drawing.Point(5, 13);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(357, 116);
            this.panel4.TabIndex = 1;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Enabled = false;
            this.richTextBox1.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(159, 25);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(189, 89);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "Pollo en salsa picosa media a base de chile de árbol y hoisin con zanahoria, pimi" +
    "ento verde, cebolla cambray y cacahuate.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(155, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 18);
            this.label6.TabIndex = 3;
            this.label6.Text = "ORIENTAL WOK";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Prototipo.Properties.Resources.menu_12;
            this.pictureBox3.Location = new System.Drawing.Point(3, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(150, 108);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // panelPrincipios
            // 
            this.panelPrincipios.AutoScroll = true;
            this.panelPrincipios.Controls.Add(this.panel17);
            this.panelPrincipios.Controls.Add(this.panel16);
            this.panelPrincipios.Controls.Add(this.panel15);
            this.panelPrincipios.Controls.Add(this.panel14);
            this.panelPrincipios.Controls.Add(this.panel13);
            this.panelPrincipios.Controls.Add(this.panel12);
            this.panelPrincipios.Controls.Add(this.panel11);
            this.panelPrincipios.Controls.Add(this.panel10);
            this.panelPrincipios.Location = new System.Drawing.Point(23, 74);
            this.panelPrincipios.Name = "panelPrincipios";
            this.panelPrincipios.Size = new System.Drawing.Size(804, 366);
            this.panelPrincipios.TabIndex = 1;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.Control;
            this.panel17.Controls.Add(this.richTextBox14);
            this.panel17.Controls.Add(this.label18);
            this.panel17.Controls.Add(this.pictureBox16);
            this.panel17.Location = new System.Drawing.Point(391, 390);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(389, 119);
            this.panel17.TabIndex = 6;
            // 
            // richTextBox14
            // 
            this.richTextBox14.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox14.Enabled = false;
            this.richTextBox14.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.richTextBox14.Location = new System.Drawing.Point(159, 25);
            this.richTextBox14.Name = "richTextBox14";
            this.richTextBox14.Size = new System.Drawing.Size(157, 89);
            this.richTextBox14.TabIndex = 0;
            this.richTextBox14.Text = "Pollo a la parilla, lechuga romana y orejona, croutones, queso parmesano y aderez" +
    "o césar.";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(155, 3);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(161, 22);
            this.label18.TabIndex = 3;
            this.label18.Text = "ENSALADA CÉSAR";
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Prototipo.Properties.Resources.cesar;
            this.pictureBox16.Location = new System.Drawing.Point(3, 3);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(150, 111);
            this.pictureBox16.TabIndex = 2;
            this.pictureBox16.TabStop = false;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.SystemColors.Control;
            this.panel16.Controls.Add(this.richTextBox13);
            this.panel16.Controls.Add(this.label17);
            this.panel16.Controls.Add(this.pictureBox15);
            this.panel16.Location = new System.Drawing.Point(9, 390);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(376, 119);
            this.panel16.TabIndex = 5;
            // 
            // richTextBox13
            // 
            this.richTextBox13.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox13.Enabled = false;
            this.richTextBox13.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.richTextBox13.Location = new System.Drawing.Point(159, 25);
            this.richTextBox13.Name = "richTextBox13";
            this.richTextBox13.Size = new System.Drawing.Size(212, 89);
            this.richTextBox13.TabIndex = 0;
            this.richTextBox13.Text = "Pechuga de pollo empanizada con salsa búfalo, lechuga romana y orejona, croutones" +
    ", aguacate, tomate, queso panela y aderezo blue cheese.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(155, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(173, 22);
            this.label17.TabIndex = 3;
            this.label17.Text = "ENSALADA BÚFALO";
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Prototipo.Properties.Resources.bufalo;
            this.pictureBox15.Location = new System.Drawing.Point(3, 3);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(150, 111);
            this.pictureBox15.TabIndex = 2;
            this.pictureBox15.TabStop = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.SystemColors.Control;
            this.panel15.Controls.Add(this.richTextBox12);
            this.panel15.Controls.Add(this.label16);
            this.panel15.Controls.Add(this.pictureBox14);
            this.panel15.Location = new System.Drawing.Point(391, 265);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(389, 119);
            this.panel15.TabIndex = 5;
            // 
            // richTextBox12
            // 
            this.richTextBox12.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox12.Enabled = false;
            this.richTextBox12.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.richTextBox12.Location = new System.Drawing.Point(159, 25);
            this.richTextBox12.Name = "richTextBox12";
            this.richTextBox12.Size = new System.Drawing.Size(223, 89);
            this.richTextBox12.TabIndex = 0;
            this.richTextBox12.Text = "Fajitas de arrachera a la plancha sobre lechuga romana y orejona, zanahoria, agua" +
    "cate, tiras de tortilla crujientes, cebolla, tomate y aderezo ranch.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(154, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(210, 22);
            this.label16.TabIndex = 3;
            this.label16.Text = "ENSALADA ARRACHERA";
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Prototipo.Properties.Resources.arrachera;
            this.pictureBox14.Location = new System.Drawing.Point(3, 3);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(150, 111);
            this.pictureBox14.TabIndex = 2;
            this.pictureBox14.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.SystemColors.Control;
            this.panel14.Controls.Add(this.richTextBox11);
            this.panel14.Controls.Add(this.label15);
            this.panel14.Controls.Add(this.pictureBox13);
            this.panel14.Location = new System.Drawing.Point(391, 140);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(389, 119);
            this.panel14.TabIndex = 5;
            // 
            // richTextBox11
            // 
            this.richTextBox11.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox11.Enabled = false;
            this.richTextBox11.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.richTextBox11.Location = new System.Drawing.Point(159, 25);
            this.richTextBox11.Name = "richTextBox11";
            this.richTextBox11.Size = new System.Drawing.Size(157, 89);
            this.richTextBox11.TabIndex = 0;
            this.richTextBox11.Text = "Pepino y pulpo.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(155, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(170, 22);
            this.label15.TabIndex = 3;
            this.label15.Text = "SUNOMONO TAKO";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Prototipo.Properties.Resources.sunomo_tako;
            this.pictureBox13.Location = new System.Drawing.Point(3, 3);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(150, 111);
            this.pictureBox13.TabIndex = 2;
            this.pictureBox13.TabStop = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.Control;
            this.panel13.Controls.Add(this.richTextBox10);
            this.panel13.Controls.Add(this.label14);
            this.panel13.Controls.Add(this.pictureBox12);
            this.panel13.Location = new System.Drawing.Point(9, 265);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(376, 119);
            this.panel13.TabIndex = 4;
            // 
            // richTextBox10
            // 
            this.richTextBox10.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox10.Enabled = false;
            this.richTextBox10.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.richTextBox10.Location = new System.Drawing.Point(159, 25);
            this.richTextBox10.Name = "richTextBox10";
            this.richTextBox10.ReadOnly = true;
            this.richTextBox10.Size = new System.Drawing.Size(212, 89);
            this.richTextBox10.TabIndex = 0;
            this.richTextBox10.Text = "Fresca ensalada de lechugas con camarones empanizados, coco, uvas, suprema de nar" +
    "anja, manzana y almendras, bañadas en mostaza dulce.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(155, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(209, 22);
            this.label14.TabIndex = 3;
            this.label14.Text = "WOK COCONUT SALAD";
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Prototipo.Properties.Resources.wok_concout;
            this.pictureBox12.Location = new System.Drawing.Point(3, 3);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(150, 111);
            this.pictureBox12.TabIndex = 2;
            this.pictureBox12.TabStop = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.Control;
            this.panel12.Controls.Add(this.richTextBox9);
            this.panel12.Controls.Add(this.label13);
            this.panel12.Controls.Add(this.pictureBox11);
            this.panel12.Location = new System.Drawing.Point(9, 140);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(376, 119);
            this.panel12.TabIndex = 4;
            // 
            // richTextBox9
            // 
            this.richTextBox9.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox9.Enabled = false;
            this.richTextBox9.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.richTextBox9.Location = new System.Drawing.Point(159, 25);
            this.richTextBox9.Name = "richTextBox9";
            this.richTextBox9.Size = new System.Drawing.Size(157, 89);
            this.richTextBox9.TabIndex = 0;
            this.richTextBox9.Text = "Pepino y camarón.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(155, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(147, 22);
            this.label13.TabIndex = 3;
            this.label13.Text = "SUNOMONO EBI";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Prototipo.Properties.Resources.sunomo_ebi;
            this.pictureBox11.Location = new System.Drawing.Point(3, 3);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(150, 111);
            this.pictureBox11.TabIndex = 2;
            this.pictureBox11.TabStop = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.Control;
            this.panel11.Controls.Add(this.richTextBox8);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.pictureBox10);
            this.panel11.Location = new System.Drawing.Point(391, 15);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(389, 119);
            this.panel11.TabIndex = 3;
            // 
            // richTextBox8
            // 
            this.richTextBox8.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox8.Enabled = false;
            this.richTextBox8.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.richTextBox8.Location = new System.Drawing.Point(159, 25);
            this.richTextBox8.Name = "richTextBox8";
            this.richTextBox8.Size = new System.Drawing.Size(157, 89);
            this.richTextBox8.TabIndex = 0;
            this.richTextBox8.Text = "Pepino y surimi.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(155, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(165, 22);
            this.label12.TabIndex = 3;
            this.label12.Text = "SUNOMONO KANI";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Prototipo.Properties.Resources.sunomo_kani;
            this.pictureBox10.Location = new System.Drawing.Point(3, 3);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(150, 111);
            this.pictureBox10.TabIndex = 2;
            this.pictureBox10.TabStop = false;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.Control;
            this.panel10.Controls.Add(this.richTextBox7);
            this.panel10.Controls.Add(this.label4);
            this.panel10.Controls.Add(this.pictureBox9);
            this.panel10.Location = new System.Drawing.Point(8, 15);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(377, 119);
            this.panel10.TabIndex = 2;
            // 
            // richTextBox7
            // 
            this.richTextBox7.BackColor = System.Drawing.SystemColors.MenuBar;
            this.richTextBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox7.Enabled = false;
            this.richTextBox7.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.richTextBox7.Location = new System.Drawing.Point(159, 25);
            this.richTextBox7.Name = "richTextBox7";
            this.richTextBox7.Size = new System.Drawing.Size(157, 89);
            this.richTextBox7.TabIndex = 0;
            this.richTextBox7.Text = "Pepino, surimi, camarón y pulpo.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 13F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(155, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 22);
            this.label4.TabIndex = 3;
            this.label4.Text = "SUNOMONO ESPECIAL";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Prototipo.Properties.Resources.sunomo_especial;
            this.pictureBox9.Location = new System.Drawing.Point(3, 3);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(150, 111);
            this.pictureBox9.TabIndex = 2;
            this.pictureBox9.TabStop = false;
            // 
            // panelPostres
            // 
            this.panelPostres.AutoScroll = true;
            this.panelPostres.Controls.Add(this.panel23);
            this.panelPostres.Controls.Add(this.panel20);
            this.panelPostres.Controls.Add(this.panel21);
            this.panelPostres.Controls.Add(this.panel19);
            this.panelPostres.Controls.Add(this.panel22);
            this.panelPostres.Controls.Add(this.panel18);
            this.panelPostres.Location = new System.Drawing.Point(23, 74);
            this.panelPostres.Name = "panelPostres";
            this.panelPostres.Size = new System.Drawing.Size(804, 366);
            this.panelPostres.TabIndex = 2;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.SystemColors.Control;
            this.panel23.Controls.Add(this.label25);
            this.panel23.Controls.Add(this.label23);
            this.panel23.Controls.Add(this.pictureBox22);
            this.panel23.Location = new System.Drawing.Point(535, 182);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(237, 167);
            this.panel23.TabIndex = 5;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(68, 139);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(92, 19);
            this.label25.TabIndex = 4;
            this.label25.Text = " en almíbar";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(44, 119);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(153, 19);
            this.label23.TabIndex = 3;
            this.label23.Text = "Flan de melocotón";
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = global::Prototipo.Properties.Resources.postre6;
            this.pictureBox22.Location = new System.Drawing.Point(3, 5);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(225, 111);
            this.pictureBox22.TabIndex = 2;
            this.pictureBox22.TabStop = false;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.SystemColors.Control;
            this.panel20.Controls.Add(this.label20);
            this.panel20.Controls.Add(this.pictureBox19);
            this.panel20.Location = new System.Drawing.Point(13, 182);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(237, 167);
            this.panel20.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(81, 139);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 19);
            this.label20.TabIndex = 3;
            this.label20.Text = "Helado ";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Prototipo.Properties.Resources.psotre4;
            this.pictureBox19.Location = new System.Drawing.Point(3, 5);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(225, 111);
            this.pictureBox19.TabIndex = 2;
            this.pictureBox19.TabStop = false;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.SystemColors.Control;
            this.panel21.Controls.Add(this.label21);
            this.panel21.Controls.Add(this.pictureBox20);
            this.panel21.Location = new System.Drawing.Point(275, 5);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(237, 168);
            this.panel21.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(30, 136);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(191, 19);
            this.label21.TabIndex = 3;
            this.label21.Text = "Fresas chinas con nata";
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::Prototipo.Properties.Resources.postre3;
            this.pictureBox20.Location = new System.Drawing.Point(9, 5);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(225, 111);
            this.pictureBox20.TabIndex = 2;
            this.pictureBox20.TabStop = false;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.Control;
            this.panel19.Controls.Add(this.label19);
            this.panel19.Controls.Add(this.pictureBox18);
            this.panel19.Location = new System.Drawing.Point(275, 182);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(237, 167);
            this.panel19.TabIndex = 4;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(55, 134);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(133, 19);
            this.label19.TabIndex = 3;
            this.label19.Text = "Flan con helado";
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::Prototipo.Properties.Resources.postre5;
            this.pictureBox18.Location = new System.Drawing.Point(3, 5);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(225, 111);
            this.pictureBox18.TabIndex = 2;
            this.pictureBox18.TabStop = false;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.SystemColors.Control;
            this.panel22.Controls.Add(this.label22);
            this.panel22.Controls.Add(this.label24);
            this.panel22.Controls.Add(this.pictureBox21);
            this.panel22.Location = new System.Drawing.Point(535, 5);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(237, 168);
            this.panel22.TabIndex = 4;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(38, 123);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(178, 19);
            this.label22.TabIndex = 3;
            this.label22.Text = "Platano frito con nata";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(78, 145);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 19);
            this.label24.TabIndex = 3;
            this.label24.Text = " y nueces";
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = global::Prototipo.Properties.Resources.postre2;
            this.pictureBox21.Location = new System.Drawing.Point(3, 5);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(225, 111);
            this.pictureBox21.TabIndex = 2;
            this.pictureBox21.TabStop = false;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.SystemColors.Control;
            this.panel18.Controls.Add(this.label5);
            this.panel18.Controls.Add(this.pictureBox17);
            this.panel18.Location = new System.Drawing.Point(12, 5);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(237, 168);
            this.panel18.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(190, 19);
            this.label5.TabIndex = 3;
            this.label5.Text = "Flan con nata y nueces";
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Prototipo.Properties.Resources.postre1;
            this.pictureBox17.Location = new System.Drawing.Point(3, 5);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(225, 111);
            this.pictureBox17.TabIndex = 2;
            this.pictureBox17.TabStop = false;
            // 
            // panelSucursales
            // 
            this.panelSucursales.AutoScroll = true;
            this.panelSucursales.BackColor = System.Drawing.SystemColors.Control;
            this.panelSucursales.Controls.Add(this.pictureBox26);
            this.panelSucursales.Controls.Add(this.pictureBox25);
            this.panelSucursales.Controls.Add(this.pictureBox24);
            this.panelSucursales.Controls.Add(this.pictureBox23);
            this.panelSucursales.Controls.Add(this.richTextBox21);
            this.panelSucursales.Controls.Add(this.richTextBox22);
            this.panelSucursales.Controls.Add(this.richTextBox19);
            this.panelSucursales.Controls.Add(this.richTextBox20);
            this.panelSucursales.Controls.Add(this.panel24);
            this.panelSucursales.Controls.Add(this.richTextBox17);
            this.panelSucursales.Controls.Add(this.richTextBox18);
            this.panelSucursales.Controls.Add(this.richTextBox15);
            this.panelSucursales.Controls.Add(this.label31);
            this.panelSucursales.Controls.Add(this.richTextBox16);
            this.panelSucursales.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panelSucursales.Location = new System.Drawing.Point(1693, -5);
            this.panelSucursales.Name = "panelSucursales";
            this.panelSucursales.Size = new System.Drawing.Size(818, 441);
            this.panelSucursales.TabIndex = 13;
            this.panelSucursales.Visible = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.Image = global::Prototipo.Properties.Resources.ub3;
            this.pictureBox26.Location = new System.Drawing.Point(670, 230);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(135, 131);
            this.pictureBox26.TabIndex = 34;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.Image = global::Prototipo.Properties.Resources.ub4;
            this.pictureBox25.Location = new System.Drawing.Point(670, 69);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(135, 131);
            this.pictureBox25.TabIndex = 33;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.Image = global::Prototipo.Properties.Resources.ub2;
            this.pictureBox24.Location = new System.Drawing.Point(273, 225);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(135, 131);
            this.pictureBox24.TabIndex = 32;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.Image = global::Prototipo.Properties.Resources.ub1;
            this.pictureBox23.Location = new System.Drawing.Point(273, 69);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(135, 131);
            this.pictureBox23.TabIndex = 31;
            this.pictureBox23.TabStop = false;
            // 
            // richTextBox21
            // 
            this.richTextBox21.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox21.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox21.Enabled = false;
            this.richTextBox21.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox21.Location = new System.Drawing.Point(439, 251);
            this.richTextBox21.Name = "richTextBox21";
            this.richTextBox21.Size = new System.Drawing.Size(212, 150);
            this.richTextBox21.TabIndex = 29;
            this.richTextBox21.Text = "Galerías Monterrey\nAv. Insurgentes 2500 L-546\nMonterrey, N.L. C. P. 64620\nTeléfon" +
    "os:\n1352-1498\n1352-1499";
            // 
            // richTextBox22
            // 
            this.richTextBox22.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox22.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox22.Enabled = false;
            this.richTextBox22.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox22.Location = new System.Drawing.Point(439, 226);
            this.richTextBox22.Name = "richTextBox22";
            this.richTextBox22.Size = new System.Drawing.Size(306, 49);
            this.richTextBox22.TabIndex = 30;
            this.richTextBox22.Text = "Wok Galerías Mty";
            // 
            // richTextBox19
            // 
            this.richTextBox19.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox19.Enabled = false;
            this.richTextBox19.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox19.Location = new System.Drawing.Point(5, 239);
            this.richTextBox19.Name = "richTextBox19";
            this.richTextBox19.Size = new System.Drawing.Size(232, 122);
            this.richTextBox19.TabIndex = 27;
            this.richTextBox19.Text = "Av. Paseo De Los Leones 1929\nCumbres 2do. Sector\nMonterrey, N. L. C.P. 64610\nTelé" +
    "fonos:\n8371-9993\n8371-9994";
            // 
            // richTextBox20
            // 
            this.richTextBox20.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox20.Enabled = false;
            this.richTextBox20.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox20.Location = new System.Drawing.Point(2, 220);
            this.richTextBox20.Name = "richTextBox20";
            this.richTextBox20.Size = new System.Drawing.Size(306, 49);
            this.richTextBox20.TabIndex = 28;
            this.richTextBox20.Text = "Wok Cumbres";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel24.Location = new System.Drawing.Point(425, 33);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(2, 400);
            this.panel24.TabIndex = 26;
            // 
            // richTextBox17
            // 
            this.richTextBox17.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox17.Enabled = false;
            this.richTextBox17.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox17.Location = new System.Drawing.Point(439, 69);
            this.richTextBox17.Name = "richTextBox17";
            this.richTextBox17.Size = new System.Drawing.Size(253, 150);
            this.richTextBox17.TabIndex = 24;
            this.richTextBox17.Text = "Plaza Regia\nAv. Eugenio Garza Sada #3720\nCol. Villa Los Pinos\nMonterrey, N.l. C.P" +
    ". 64770\nTeléfonos:\n8103-0931\n8103-0160";
            // 
            // richTextBox18
            // 
            this.richTextBox18.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox18.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox18.Enabled = false;
            this.richTextBox18.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox18.Location = new System.Drawing.Point(439, 45);
            this.richTextBox18.Name = "richTextBox18";
            this.richTextBox18.Size = new System.Drawing.Size(212, 49);
            this.richTextBox18.TabIndex = 25;
            this.richTextBox18.Text = "Wok Contry";
            // 
            // richTextBox15
            // 
            this.richTextBox15.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox15.Enabled = false;
            this.richTextBox15.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox15.Location = new System.Drawing.Point(5, 65);
            this.richTextBox15.Name = "richTextBox15";
            this.richTextBox15.Size = new System.Drawing.Size(306, 150);
            this.richTextBox15.TabIndex = 22;
            this.richTextBox15.Text = "Plaza La Joya De Anáhuac\nAve. Manuel L. Barragán #550 L-110\nCol. Residencial Anáh" +
    "uac\nSan Nicolás De Los Garza, N. L. C.P. 66457\nTeléfonos:\n8376-2520\n1133-6961";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.SystemColors.Control;
            this.label31.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.label31.Location = new System.Drawing.Point(279, 6);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(305, 25);
            this.label31.TabIndex = 21;
            this.label31.Text = "Conozca nuestras sucursales";
            // 
            // richTextBox16
            // 
            this.richTextBox16.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox16.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox16.Enabled = false;
            this.richTextBox16.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox16.Location = new System.Drawing.Point(2, 45);
            this.richTextBox16.Name = "richTextBox16";
            this.richTextBox16.Size = new System.Drawing.Size(306, 49);
            this.richTextBox16.TabIndex = 23;
            this.richTextBox16.Text = "Wok Anáhuac\n";
            // 
            // panelInicio
            // 
            this.panelInicio.AutoScroll = true;
            this.panelInicio.BackColor = System.Drawing.SystemColors.Control;
            this.panelInicio.Controls.Add(this.panelMenu);
            this.panelInicio.Controls.Add(this.panelRegistro);
            this.panelInicio.Controls.Add(this.panelSucursales);
            this.panelInicio.Controls.Add(this.panelOrdena);
            this.panelInicio.Controls.Add(this.panelContacto);
            this.panelInicio.Controls.Add(this.button2);
            this.panelInicio.Controls.Add(this.button3);
            this.panelInicio.Controls.Add(this.button4);
            this.panelInicio.Controls.Add(this.button5);
            this.panelInicio.Location = new System.Drawing.Point(181, 95);
            this.panelInicio.Name = "panelInicio";
            this.panelInicio.Size = new System.Drawing.Size(853, 468);
            this.panelInicio.TabIndex = 9;
            this.panelInicio.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // panelRegistro
            // 
            this.panelRegistro.Controls.Add(this.label41);
            this.panelRegistro.Controls.Add(this.txtRegCorreo);
            this.panelRegistro.Controls.Add(this.label40);
            this.panelRegistro.Controls.Add(this.txtRegConPass);
            this.panelRegistro.Controls.Add(this.btnRegistro);
            this.panelRegistro.Controls.Add(this.label37);
            this.panelRegistro.Controls.Add(this.label38);
            this.panelRegistro.Controls.Add(this.txtRegPass);
            this.panelRegistro.Controls.Add(this.txtRegUser);
            this.panelRegistro.Controls.Add(this.label39);
            this.panelRegistro.Location = new System.Drawing.Point(24, 599);
            this.panelRegistro.Name = "panelRegistro";
            this.panelRegistro.Size = new System.Drawing.Size(822, 441);
            this.panelRegistro.TabIndex = 30;
            this.panelRegistro.Visible = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label41.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label41.Location = new System.Drawing.Point(365, 281);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(85, 26);
            this.label41.TabIndex = 30;
            this.label41.Text = "Correo";
            // 
            // txtRegCorreo
            // 
            this.txtRegCorreo.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegCorreo.Location = new System.Drawing.Point(251, 314);
            this.txtRegCorreo.Name = "txtRegCorreo";
            this.txtRegCorreo.Size = new System.Drawing.Size(308, 31);
            this.txtRegCorreo.TabIndex = 30;
            this.txtRegCorreo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRegCorreo_KeyPress);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label40.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label40.Location = new System.Drawing.Point(287, 211);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(248, 26);
            this.label40.TabIndex = 28;
            this.label40.Text = "Confirmar Contraseña";
            // 
            // txtRegConPass
            // 
            this.txtRegConPass.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegConPass.Location = new System.Drawing.Point(251, 244);
            this.txtRegConPass.Name = "txtRegConPass";
            this.txtRegConPass.Size = new System.Drawing.Size(308, 31);
            this.txtRegConPass.TabIndex = 29;
            this.txtRegConPass.UseSystemPasswordChar = true;
            // 
            // btnRegistro
            // 
            this.btnRegistro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnRegistro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegistro.FlatAppearance.BorderSize = 0;
            this.btnRegistro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistro.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.btnRegistro.ForeColor = System.Drawing.SystemColors.Control;
            this.btnRegistro.Image = global::Prototipo.Properties.Resources.login2;
            this.btnRegistro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistro.Location = new System.Drawing.Point(298, 372);
            this.btnRegistro.Name = "btnRegistro";
            this.btnRegistro.Size = new System.Drawing.Size(207, 43);
            this.btnRegistro.TabIndex = 31;
            this.btnRegistro.Text = "Registrar";
            this.btnRegistro.UseVisualStyleBackColor = false;
            this.btnRegistro.Click += new System.EventHandler(this.btnRegistro_Click_1);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label37.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label37.Location = new System.Drawing.Point(340, 137);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(137, 26);
            this.label37.TabIndex = 25;
            this.label37.Text = "Contraseña";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label38.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label38.Location = new System.Drawing.Point(360, 60);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(90, 26);
            this.label38.TabIndex = 24;
            this.label38.Text = "Usuario";
            // 
            // txtRegPass
            // 
            this.txtRegPass.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegPass.Location = new System.Drawing.Point(251, 170);
            this.txtRegPass.Name = "txtRegPass";
            this.txtRegPass.Size = new System.Drawing.Size(308, 31);
            this.txtRegPass.TabIndex = 28;
            this.txtRegPass.UseSystemPasswordChar = true;
            // 
            // txtRegUser
            // 
            this.txtRegUser.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegUser.Location = new System.Drawing.Point(251, 98);
            this.txtRegUser.Name = "txtRegUser";
            this.txtRegUser.Size = new System.Drawing.Size(308, 31);
            this.txtRegUser.TabIndex = 27;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label39.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label39.Location = new System.Drawing.Point(305, 21);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(215, 26);
            this.label39.TabIndex = 21;
            this.label39.Text = "Registro de usuario";
            // 
            // panelOrdena
            // 
            this.panelOrdena.Controls.Add(this.button7);
            this.panelOrdena.Controls.Add(this.btn_login);
            this.panelOrdena.Controls.Add(this.label28);
            this.panelOrdena.Controls.Add(this.label27);
            this.panelOrdena.Controls.Add(this.txtPass);
            this.panelOrdena.Controls.Add(this.txtUser);
            this.panelOrdena.Controls.Add(this.label26);
            this.panelOrdena.Location = new System.Drawing.Point(1064, 703);
            this.panelOrdena.Name = "panelOrdena";
            this.panelOrdena.Size = new System.Drawing.Size(822, 441);
            this.panelOrdena.TabIndex = 28;
            this.panelOrdena.Visible = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Maroon;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.Control;
            this.button7.Image = global::Prototipo.Properties.Resources.login2;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(282, 337);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(261, 43);
            this.button7.TabIndex = 27;
            this.button7.Text = "Registrar Ahora";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btn_login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_login.FlatAppearance.BorderSize = 0;
            this.btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_login.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_login.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_login.Image = global::Prototipo.Properties.Resources.login2;
            this.btn_login.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_login.Location = new System.Drawing.Point(305, 239);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(207, 43);
            this.btn_login.TabIndex = 24;
            this.btn_login.Text = "Iniciar Sesión";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label28.Location = new System.Drawing.Point(340, 161);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(130, 25);
            this.label28.TabIndex = 25;
            this.label28.Text = "Contraseña";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label27.Location = new System.Drawing.Point(365, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(87, 25);
            this.label27.TabIndex = 24;
            this.label27.Text = "Usuario";
            // 
            // txtPass
            // 
            this.txtPass.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPass.Location = new System.Drawing.Point(251, 191);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(308, 31);
            this.txtPass.TabIndex = 23;
            this.txtPass.UseSystemPasswordChar = true;
            this.txtPass.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPass_KeyPress_1);
            // 
            // txtUser
            // 
            this.txtUser.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.Location = new System.Drawing.Point(251, 98);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(308, 31);
            this.txtUser.TabIndex = 22;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label26.Location = new System.Drawing.Point(321, 299);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(184, 25);
            this.label26.TabIndex = 21;
            this.label26.Text = "¿Nuevo Usuario?";
            // 
            // panelContacto
            // 
            this.panelContacto.AutoScroll = true;
            this.panelContacto.BackColor = System.Drawing.SystemColors.Control;
            this.panelContacto.Controls.Add(this.button1);
            this.panelContacto.Controls.Add(this.textBox7);
            this.panelContacto.Controls.Add(this.label36);
            this.panelContacto.Controls.Add(this.textBox6);
            this.panelContacto.Controls.Add(this.label35);
            this.panelContacto.Controls.Add(this.textBox5);
            this.panelContacto.Controls.Add(this.label34);
            this.panelContacto.Controls.Add(this.textBox4);
            this.panelContacto.Controls.Add(this.label33);
            this.panelContacto.Controls.Add(this.textBox3);
            this.panelContacto.Controls.Add(this.label32);
            this.panelContacto.Controls.Add(this.richTextBox23);
            this.panelContacto.Controls.Add(this.label30);
            this.panelContacto.Controls.Add(this.label29);
            this.panelContacto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panelContacto.Location = new System.Drawing.Point(1966, 878);
            this.panelContacto.Name = "panelContacto";
            this.panelContacto.Size = new System.Drawing.Size(848, 453);
            this.panelContacto.TabIndex = 29;
            this.panelContacto.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(436, 609);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(310, 43);
            this.button1.TabIndex = 36;
            this.button1.Text = "Enviar comentarios";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(436, 465);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(310, 132);
            this.textBox7.TabIndex = 35;
            this.textBox7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox7_KeyPress_1);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.SystemColors.Control;
            this.label36.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.label36.Location = new System.Drawing.Point(521, 436);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(149, 26);
            this.label36.TabIndex = 34;
            this.label36.Text = "Comentarios";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(436, 394);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(310, 31);
            this.textBox6.TabIndex = 33;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.SystemColors.Control;
            this.label35.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.label35.Location = new System.Drawing.Point(545, 361);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 26);
            this.label35.TabIndex = 32;
            this.label35.Text = "Sucursal";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(556, 322);
            this.textBox5.MaxLength = 2;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(67, 31);
            this.textBox5.TabIndex = 31;
            this.textBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox5_KeyPress);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.SystemColors.Control;
            this.label34.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.label34.Location = new System.Drawing.Point(560, 284);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 26);
            this.label34.TabIndex = 30;
            this.label34.Text = "Edad";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(436, 238);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(310, 31);
            this.textBox4.TabIndex = 29;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.SystemColors.Control;
            this.label33.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.label33.Location = new System.Drawing.Point(551, 205);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 26);
            this.label33.TabIndex = 28;
            this.label33.Text = "Correo";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(436, 166);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(310, 31);
            this.textBox3.TabIndex = 27;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.SystemColors.Control;
            this.label32.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.label32.Location = new System.Drawing.Point(544, 133);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(99, 26);
            this.label32.TabIndex = 26;
            this.label32.Text = "Nombre";
            // 
            // richTextBox23
            // 
            this.richTextBox23.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox23.Enabled = false;
            this.richTextBox23.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Italic);
            this.richTextBox23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.richTextBox23.Location = new System.Drawing.Point(56, 133);
            this.richTextBox23.Name = "richTextBox23";
            this.richTextBox23.Size = new System.Drawing.Size(355, 498);
            this.richTextBox23.TabIndex = 25;
            this.richTextBox23.Text = resources.GetString("richTextBox23.Text");
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.SystemColors.Control;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold);
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.label30.Location = new System.Drawing.Point(248, 84);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(341, 26);
            this.label30.TabIndex = 22;
            this.label30.Text = "Cuentanos tu experiencia Wok";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.SystemColors.Control;
            this.label29.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold);
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.label29.Location = new System.Drawing.Point(341, 27);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(162, 38);
            this.label29.TabIndex = 21;
            this.label29.Text = "Contacto";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button2.Image = global::Prototipo.Properties.Resources.login2;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(116, 251);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(203, 97);
            this.button2.TabIndex = 10;
            this.button2.Text = "     Inicia Sesión";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.btn_Ord_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button3.Image = global::Prototipo.Properties.Resources.phone_call;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(531, 256);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(203, 97);
            this.button3.TabIndex = 9;
            this.button3.Text = "    Contacto";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.btn_Cont_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button4.Image = global::Prototipo.Properties.Resources.restaurant;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(531, 78);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(203, 97);
            this.button4.TabIndex = 8;
            this.button4.Text = "    Sucursales";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.btn_Sucursales_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.button5.Image = global::Prototipo.Properties.Resources.menu1;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(116, 78);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(203, 97);
            this.button5.TabIndex = 7;
            this.button5.Text = "    Menú";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.btn_Menu_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.CancelButton = this.btn_Cerrar;
            this.ClientSize = new System.Drawing.Size(1039, 566);
            this.Controls.Add(this.pan_head);
            this.Controls.Add(this.panelInicio);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel3);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.Main_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            this.panelBebidas.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            this.panelPFuerte.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panelPrincipios.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.panelPostres.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            this.panelSucursales.ResumeLayout(false);
            this.panelSucursales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            this.panelInicio.ResumeLayout(false);
            this.panelRegistro.ResumeLayout(false);
            this.panelRegistro.PerformLayout();
            this.panelOrdena.ResumeLayout(false);
            this.panelOrdena.PerformLayout();
            this.panelContacto.ResumeLayout(false);
            this.panelContacto.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pan_head;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_Home;
        private System.Windows.Forms.Panel pan_Marca;
        private System.Windows.Forms.Button btn_Ord;
        private System.Windows.Forms.Button btn_Cont;
        private System.Windows.Forms.Button btn_Sucursales;
        private System.Windows.Forms.Button btn_Menu;
        private System.Windows.Forms.Button btn_Cerrar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.LinkLabel labelPFuerte;
        private System.Windows.Forms.LinkLabel labelPostres;
        private System.Windows.Forms.LinkLabel labelPrincipios;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelPostres;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Panel panelPrincipios;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.RichTextBox richTextBox14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.RichTextBox richTextBox13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.RichTextBox richTextBox12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.RichTextBox richTextBox11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.RichTextBox richTextBox10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.RichTextBox richTextBox9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.RichTextBox richTextBox8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.RichTextBox richTextBox7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Panel panelSucursales;
        private System.Windows.Forms.RichTextBox richTextBox21;
        private System.Windows.Forms.RichTextBox richTextBox22;
        private System.Windows.Forms.RichTextBox richTextBox19;
        private System.Windows.Forms.RichTextBox richTextBox20;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.RichTextBox richTextBox17;
        private System.Windows.Forms.RichTextBox richTextBox18;
        private System.Windows.Forms.RichTextBox richTextBox15;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.RichTextBox richTextBox16;
        private System.Windows.Forms.Panel panelInicio;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Panel panelRegistro;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtRegCorreo;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtRegConPass;
        private System.Windows.Forms.Button btnRegistro;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtRegPass;
        private System.Windows.Forms.TextBox txtRegUser;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panelOrdena;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panelContacto;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.RichTextBox richTextBox23;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.LinkLabel labelBebidas;
        private System.Windows.Forms.Panel panelBebidas;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.RichTextBox richTextBox25;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.RichTextBox richTextBox26;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.RichTextBox richTextBox27;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.RichTextBox richTextBox28;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.RichTextBox richTextBox29;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.Panel panelPFuerte;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RichTextBox richTextBox6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.RichTextBox richTextBox5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RichTextBox richTextBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RichTextBox richTextBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}
