﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public static class OrdenGlobal
    {
        private static String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        private static int _idOrden;
        private static string _fecha="";
        private static string _hora = "";
        private static double _montoTotal;
        private static List<Persona> _ordenGlobal = new List<Persona>();
        private static List<Item> _ordenAgrupada = new List<Item>();
        private static int _currentPerson = 0;
        private static int _idCliente;

        public static List<Persona> ordenGlobal
        {
            get { return _ordenGlobal; }
            set { _ordenGlobal = value; }
        }

        public static int currentPerson
        {
            get { return _currentPerson; }
            set { _currentPerson = value; }
        }

        public static int idCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }

        public static int idOrden
        {
            get { return _idOrden; }
            set { _idOrden = value; }
        }

        public static string fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }

        public static string hora
        {
            get { return _hora; }
            set { _hora = value; }
        }

        public static double montoTotal()
        {
                recalcularMonto(); 
               return _montoTotal; 
        }

        private static void recalcularMonto()
        {
            double x = 0;
            foreach (Persona p in _ordenGlobal)
            {
                x = x + p.monto();
            }
            _montoTotal = x;
        }

        public static void guardarOrden()
        {
            recalcularMonto();
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Orden WHERE IdOrden=0";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            DataRow dr = dt.NewRow();
            dr["Estado"] = "0";
            dr["Fecha"] = _fecha;
            dr["Hora"] = _hora;
            dr["MontoTotal"] = _montoTotal.ToString();
            dr["idCliente"] = _idCliente;
            dt.Rows.Add(dr);
            OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
            da.InsertCommand = cb.GetInsertCommand();
            da.Update(dt);
            dt.AcceptChanges();
            dt.Clear();
            dt.Columns.Clear();
            dt.Rows.Clear();
            str = "SELECT TOP 1 IdOrden FROM Orden ORDER BY IdOrden DESC";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
                _idOrden = Int32.Parse(dt.Rows[0]["IdOrden"].ToString());
            foreach (Persona p in ordenGlobal)
            {
                p.guardarOrden(idOrden);
            }
        }
    }
}