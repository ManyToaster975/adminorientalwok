﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Prototipo
{
    public partial class Cantidad : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        static Cantidad cant;
        static int cantidad;
        public Cantidad(string articulo)
        {
            InitializeComponent();
            lblArt.Text = articulo;
        }

        public static int Show(string articulo)
        {
            cant = new Cantidad(articulo);
            cant.ShowDialog();
            return cantidad;
        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            int can = Int32.Parse(label1.Text);
            if (can > 1)
                can--;
            label1.Text = can.ToString();
        }

        private void btnMas_Click(object sender, EventArgs e)
        {
            int can = Int32.Parse(label1.Text);
            can++;
            label1.Text = can.ToString();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            cantidad = 0;
            this.Dispose();
        }

        private void btnAñadir_Click(object sender, EventArgs e)
        {
            cantidad = Int32.Parse(label1.Text);
            this.Dispose();
        }

        private void Cantidad_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
