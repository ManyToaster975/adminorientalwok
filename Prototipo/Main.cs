﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public partial class Main : Form
    {
        public static String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        public Main()
        {
            InitializeComponent();
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public static EnviarEmail enviarEmail;

        public void ocultar(){
            panelMenu.Visible = false;
            panelOrdena.Visible = false;
            panelSucursales.Visible = false;
            panelContacto.Visible = false;
            panelRegistro.Visible = false;
        }
        private void btn_Cerrar_Click(object sender, EventArgs e)
        {
            //Desicion Si/No
            DialogResult dg = MessageBox.Show("Desea salir del sistema?", "AVISO", MessageBoxButtons.YesNo);
            if (dg == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_Home_Click(object sender, EventArgs e)
        {
            pan_Marca.Location = new Point(0, btn_Home.Location.Y);
            ocultar();
        }

        private void btn_Menu_Click(object sender, EventArgs e)
        {
            pan_Marca.Location = new Point(0, btn_Menu.Location.Y);
            ocultar();
            panelMenu.Visible = true;
            panelMenu.Location = new Point(0, 0);
        }

        private void btn_Ord_Click(object sender, EventArgs e)
        {
            pan_Marca.Location = new Point(0, btn_Ord.Location.Y);
            ocultar();
            panelOrdena.Visible = true;
            panelOrdena.Location = new Point(0, 0);

        }

        private void btn_Sucursales_Click(object sender, EventArgs e)
        {
            pan_Marca.Location = new Point(0, btn_Sucursales.Location.Y);
            ocultar();
            panelSucursales.Visible = true;
            panelSucursales.Location = new Point(0, 0);
        }

        private void btn_Cont_Click(object sender, EventArgs e)
        {
            pan_Marca.Location = new Point(0, btn_Cont.Location.Y);
            ocultar();
            panelContacto.Visible = true;
            panelContacto.Location = new Point(0, 0);
        }

        private void Main_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void labelPrincipios_Click(object sender, EventArgs e)
        {
            panelPFuerte.Visible = false;
            panelPostres.Visible = false;
            panelBebidas.Visible = false;
            panelPrincipios.Visible = true;
        }

        private void labelPFuerte_Click(object sender, EventArgs e)
        {
            panelPFuerte.Visible = true;
            panelPostres.Visible = false;
            panelBebidas.Visible = false;
            panelPrincipios.Visible = false;
        }

        private void labelPostres_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            panelPostres.Visible = true;
            panelPFuerte.Visible = false;
            panelBebidas.Visible = false;
            panelPrincipios.Visible = false;
        }

        private void labelBebidas_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            panelPFuerte.Visible = false;
            panelPrincipios.Visible = false;
            panelPostres.Visible = false;
            panelBebidas.Visible = true;
        }
        

        private void btn_login_Click(object sender, EventArgs e)
        {
            reconocimientoSesion();
        }

        private void btnRegistro_Click_1(object sender, EventArgs e)
        {
            registrarUsuario();
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            ocultar();
            panelRegistro.Visible = true;
            panelRegistro.Location = new Point(0, 0);

        }

        

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char chr = e.KeyChar;
            if(!Char.IsDigit(chr) && chr != 8)
            {
                e.Handled = true;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //Se requiere un servidor SMTP y una cuenta real para habilitar el servicio
            String msj = textBox7.ToString();
            //enviarEmail = new EnviarEmail();
            //enviarEmail.enviarEmail("correo_destino", "Asunto", msj);
            MessageBox.Show("Lo sentimos, por el momento el servicio no esta disponible, vuelva a intentarlo mas tarde.");
        }

        private void txtPass_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            //Al presionar enter permite el acceso
            if (e.KeyChar == (char)13)
            {
                reconocimientoSesion();
            }
        }

        private void txtRegCorreo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                registrarUsuario();
            }
        }

        //Metodos
        private void reconocimientoSesion()
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Usuario WHERE Usuario='" + txtUser.Text + "' AND Cont='" + txtPass.Text + "'";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                int id = Int32.Parse(dt.Rows[0]["IdUsuario"].ToString());
                MessageBox.Show("Bienvenido");
                Menu ord = new Menu(this, id);
                ord.Show();
                this.Hide();
            }
            else
                MessageBox.Show("Incorrecto");
        }

        private void registrarUsuario()
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Usuario WHERE IdUsuario=0";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (txtRegUser.Text == "")
            {
                MessageBox.Show("No puede dejar el usuario vacío");
                txtRegUser.Focus();
                /*
                if (dt.Rows[0]["Correcto"].ToString() == "1")
                {
                    MessageBox.Show("Usuario Registrado");
                    ocultar();
                    pan_Marca.Location = new Point(0, btn_Home.Location.Y);
                }
                else
                    MessageBox.Show("Incorrecto");
                    */
            }
            else if (txtRegPass.Text == "")
            {
                MessageBox.Show("No puede dejar la contraseña vacío");
                txtRegUser.Focus();
            }
            else if (txtRegPass.Text != txtRegConPass.Text)
            {
                MessageBox.Show("No coinciden las contraseñas");
                txtRegConPass.Focus();
            }
            else if (txtRegCorreo.Text == "")
            {
                MessageBox.Show("No puede dejar el correo vacío");
                txtRegCorreo.Focus();
            }
            else
            {
                DataRow dr = dt.NewRow();
                dr["Usuario"] = txtRegUser.Text;
                dr["Cont"] = txtRegPass.Text;
                dr["Tipo"] = "1";
                dt.Rows.Add(dr);
                OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
                da.InsertCommand = cb.GetInsertCommand();
                da.Update(dt);
                dt.AcceptChanges();
                MessageBox.Show("Usuario Registrado");
                ocultar();
                pan_Marca.Location = new Point(0, btn_Home.Location.Y);
            }
        }

        private void textBox7_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            //MessageBox.Show("Lo sentimos, por el momento el servicio no esta disponible, vuelva a intentarlo mas tarde.");
        }
    }
}