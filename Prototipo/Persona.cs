﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public class Persona
    {
        private String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        public string nombre;
        public int edad;
        private int _idPersona;
        public List<Item> orden = new List<Item>();
        private double _monto;
        public Persona(string nombre, int edad)
        {
            this.edad = edad;
            this.nombre = nombre;
        }

        public int idPersona
        {
            get { return _idPersona; }
            set { _idPersona = value; }
        }

        public void addArticulo(int clave, int cantidad, string nombre, int costo)
        {
            orden.Add(new Item(clave, cantidad, nombre, costo));
            recalcularMonto();
        }

        public double monto()
        {
            recalcularMonto();
            return _monto;
        }

        private void recalcularMonto()
        {
            double x = 0;
            foreach (Item item in orden)
            {
                x = x + item.monto;
            }
            _monto = x;
        }

        public void guardarOrden(int IdOrden)
        {
            recalcularMonto();
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Persona WHERE IdPersona=0";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            DataRow dr = dt.NewRow();
            dr["Nombre"] = nombre;
            dr["Edad"] = edad;
            dr["IdOrden"] =IdOrden;
            dr["Monto"] = _monto;
            dt.Rows.Add(dr);
            OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
            da.InsertCommand = cb.GetInsertCommand();
            da.Update(dt);
            dt.AcceptChanges();
            dt.Clear();
            dt.Columns.Clear();
            dt.Rows.Clear();
            str = "SELECT TOP 1 IdPersona FROM Persona ORDER BY IdPersona DESC";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
                _idPersona = Int32.Parse(dt.Rows[0]["IdPersona"].ToString());
            foreach (Item item in orden)
            {
                item.guardarDetalle(idPersona);
            }
        }
    }
}