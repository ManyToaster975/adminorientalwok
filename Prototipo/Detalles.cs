﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public partial class Detalles : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        int idOrden;
        Pedido pedido;
        public static String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        public Detalles(int idOrden, Pedido pedido)
        {
            InitializeComponent();
            this.idOrden = idOrden;
            this.pedido = pedido;
        }

        private void Detalles_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            String qry = "SELECT * FROM PedidoDetalle WHERE idOrden=" + idOrden.ToString();
            OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter(qry, cnn);
            oleDbDataAdapter.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                lblNombre.Text = dt.Rows[0]["Cliente"].ToString();
                lblOrden.Text = dt.Rows[0]["IdOrden"].ToString();
                lblMonto.Text = String.Format("${0:n}", dt.Rows[0]["MontoTotal"].ToString());
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    dataGridView1.Rows.Add();
                    foreach(DataGridViewColumn col in dataGridView1.Columns)
                    {
                        dataGridView1.Rows[i].Cells[col.Name].Value = dt.Rows[i][col.Name].ToString();
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OrdenCancelada(idOrden);
            MessageBox.Show("Orden Cancelada");
            this.Dispose();
        }

        public void OrdenCancelada(int idOrden)
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT IdOrden, Estado FROM Orden WHERE IdOrden=" + idOrden.ToString();
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                dt.Rows[0]["Estado"] = "2";
                OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
                da.UpdateCommand = cb.GetUpdateCommand();
                da.Update(dt);
                dt.AcceptChanges();
            }
        }

        private void Detalles_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btn_Cerrar_Click(object sender, EventArgs e)
        {
            pedido.cargarOrden();
            this.Dispose();
        }
    }
}
