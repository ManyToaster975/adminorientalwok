﻿namespace Prototipo
{
    partial class Pedido
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOrden = new System.Windows.Forms.Label();
            this.lblOrden1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblMonto = new System.Windows.Forms.Label();
            this.lblMonto1 = new System.Windows.Forms.Label();
            this.lblEstatus = new System.Windows.Forms.Label();
            this.lblEstatus1 = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.lblHora1 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.lblFecha1 = new System.Windows.Forms.Label();
            this.btnDetalle = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblOrden
            // 
            this.lblOrden.AutoSize = true;
            this.lblOrden.Font = new System.Drawing.Font("Century Gothic", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrden.ForeColor = System.Drawing.Color.White;
            this.lblOrden.Location = new System.Drawing.Point(87, 7);
            this.lblOrden.Name = "lblOrden";
            this.lblOrden.Size = new System.Drawing.Size(16, 17);
            this.lblOrden.TabIndex = 1;
            this.lblOrden.Text = "0";
            // 
            // lblOrden1
            // 
            this.lblOrden1.AutoSize = true;
            this.lblOrden1.Font = new System.Drawing.Font("Century Gothic", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrden1.ForeColor = System.Drawing.Color.White;
            this.lblOrden1.Location = new System.Drawing.Point(14, 7);
            this.lblOrden1.Name = "lblOrden1";
            this.lblOrden1.Size = new System.Drawing.Size(67, 17);
            this.lblOrden1.TabIndex = 0;
            this.lblOrden1.Text = "Orden #:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.splitContainer1.Panel1.Controls.Add(this.lblMonto);
            this.splitContainer1.Panel1.Controls.Add(this.lblMonto1);
            this.splitContainer1.Panel1.Controls.Add(this.lblEstatus);
            this.splitContainer1.Panel1.Controls.Add(this.lblEstatus1);
            this.splitContainer1.Panel1.Controls.Add(this.lblHora);
            this.splitContainer1.Panel1.Controls.Add(this.lblHora1);
            this.splitContainer1.Panel1.Controls.Add(this.lblFecha);
            this.splitContainer1.Panel1.Controls.Add(this.lblFecha1);
            this.splitContainer1.Panel1.Controls.Add(this.lblOrden1);
            this.splitContainer1.Panel1.Controls.Add(this.lblOrden);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.splitContainer1.Panel2.Controls.Add(this.btnDetalle);
            this.splitContainer1.Size = new System.Drawing.Size(165, 315);
            this.splitContainer1.SplitterDistance = 156;
            this.splitContainer1.TabIndex = 11;
            // 
            // lblMonto
            // 
            this.lblMonto.AutoSize = true;
            this.lblMonto.Font = new System.Drawing.Font("Century Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonto.ForeColor = System.Drawing.Color.White;
            this.lblMonto.Location = new System.Drawing.Point(64, 135);
            this.lblMonto.Name = "lblMonto";
            this.lblMonto.Size = new System.Drawing.Size(22, 16);
            this.lblMonto.TabIndex = 17;
            this.lblMonto.Text = "$0";
            // 
            // lblMonto1
            // 
            this.lblMonto1.AutoSize = true;
            this.lblMonto1.Font = new System.Drawing.Font("Century Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonto1.ForeColor = System.Drawing.Color.White;
            this.lblMonto1.Location = new System.Drawing.Point(14, 135);
            this.lblMonto1.Name = "lblMonto1";
            this.lblMonto1.Size = new System.Drawing.Size(52, 16);
            this.lblMonto1.TabIndex = 16;
            this.lblMonto1.Text = "Monto: ";
            // 
            // lblEstatus
            // 
            this.lblEstatus.AutoSize = true;
            this.lblEstatus.Font = new System.Drawing.Font("Century Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstatus.ForeColor = System.Drawing.Color.White;
            this.lblEstatus.Location = new System.Drawing.Point(64, 104);
            this.lblEstatus.Name = "lblEstatus";
            this.lblEstatus.Size = new System.Drawing.Size(26, 16);
            this.lblEstatus.TabIndex = 15;
            this.lblEstatus.Text = "NA";
            // 
            // lblEstatus1
            // 
            this.lblEstatus1.AutoSize = true;
            this.lblEstatus1.Font = new System.Drawing.Font("Century Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstatus1.ForeColor = System.Drawing.Color.White;
            this.lblEstatus1.Location = new System.Drawing.Point(14, 104);
            this.lblEstatus1.Name = "lblEstatus1";
            this.lblEstatus1.Size = new System.Drawing.Size(52, 16);
            this.lblEstatus1.TabIndex = 14;
            this.lblEstatus1.Text = "Estatus:";
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Font = new System.Drawing.Font("Century Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.White;
            this.lblHora.Location = new System.Drawing.Point(64, 77);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(39, 16);
            this.lblHora.TabIndex = 13;
            this.lblHora.Text = "00:00";
            // 
            // lblHora1
            // 
            this.lblHora1.AutoSize = true;
            this.lblHora1.Font = new System.Drawing.Font("Century Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora1.ForeColor = System.Drawing.Color.White;
            this.lblHora1.Location = new System.Drawing.Point(14, 77);
            this.lblHora1.Name = "lblHora1";
            this.lblHora1.Size = new System.Drawing.Size(42, 16);
            this.lblHora1.TabIndex = 12;
            this.lblHora1.Text = "Hora: ";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Century Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.ForeColor = System.Drawing.Color.White;
            this.lblFecha.Location = new System.Drawing.Point(64, 44);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(74, 16);
            this.lblFecha.TabIndex = 11;
            this.lblFecha.Text = "0000-00-00";
            // 
            // lblFecha1
            // 
            this.lblFecha1.AutoSize = true;
            this.lblFecha1.Font = new System.Drawing.Font("Century Gothic", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha1.ForeColor = System.Drawing.Color.White;
            this.lblFecha1.Location = new System.Drawing.Point(14, 44);
            this.lblFecha1.Name = "lblFecha1";
            this.lblFecha1.Size = new System.Drawing.Size(50, 16);
            this.lblFecha1.TabIndex = 10;
            this.lblFecha1.Text = "Fecha: ";
            // 
            // btnDetalle
            // 
            this.btnDetalle.BackColor = System.Drawing.Color.Green;
            this.btnDetalle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDetalle.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDetalle.ForeColor = System.Drawing.Color.White;
            this.btnDetalle.Location = new System.Drawing.Point(-13, 3);
            this.btnDetalle.Name = "btnDetalle";
            this.btnDetalle.Size = new System.Drawing.Size(185, 41);
            this.btnDetalle.TabIndex = 0;
            this.btnDetalle.Text = "Abrir Detalles";
            this.btnDetalle.UseVisualStyleBackColor = false;
            this.btnDetalle.Click += new System.EventHandler(this.btnDetalle_Click);
            // 
            // Pedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "Pedido";
            this.Size = new System.Drawing.Size(168, 210);
            this.Load += new System.EventHandler(this.Pedido_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblOrden;
        private System.Windows.Forms.Label lblOrden1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label lblMonto;
        private System.Windows.Forms.Label lblMonto1;
        private System.Windows.Forms.Label lblEstatus;
        private System.Windows.Forms.Label lblEstatus1;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Label lblHora1;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label lblFecha1;
        private System.Windows.Forms.Button btnDetalle;
    }
}
