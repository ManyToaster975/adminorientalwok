﻿namespace Prototipo
{
    partial class PedidoDetalle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pan_head = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCantidadMontoTotal = new System.Windows.Forms.Label();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.lblMontoTotal = new System.Windows.Forms.Label();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.lblTextoNombre = new System.Windows.Forms.Label();
            this.lblEdad = new System.Windows.Forms.Label();
            this.lblOrden = new System.Windows.Forms.Label();
            this.lblSaludo = new System.Windows.Forms.Label();
            this.lblNoOrden = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Cancelar = new System.Windows.Forms.Button();
            this.lblEstatus = new System.Windows.Forms.Label();
            this.lblTipoEstatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Cerrar = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pan_head
            // 
            this.pan_head.BackColor = System.Drawing.Color.Red;
            this.pan_head.Dock = System.Windows.Forms.DockStyle.Top;
            this.pan_head.Location = new System.Drawing.Point(0, 0);
            this.pan_head.Margin = new System.Windows.Forms.Padding(0);
            this.pan_head.Name = "pan_head";
            this.pan_head.Size = new System.Drawing.Size(800, 10);
            this.pan_head.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel1.Controls.Add(this.lblTipoEstatus);
            this.panel1.Controls.Add(this.lblEstatus);
            this.panel1.Controls.Add(this.lblNombre);
            this.panel1.Controls.Add(this.lblNoOrden);
            this.panel1.Controls.Add(this.lblCantidadMontoTotal);
            this.panel1.Controls.Add(this.lblDireccion);
            this.panel1.Controls.Add(this.lblMontoTotal);
            this.panel1.Controls.Add(this.lblTelefono);
            this.panel1.Controls.Add(this.lblTextoNombre);
            this.panel1.Controls.Add(this.lblEdad);
            this.panel1.Controls.Add(this.lblOrden);
            this.panel1.Controls.Add(this.lblSaludo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(303, 440);
            this.panel1.TabIndex = 6;
            // 
            // lblCantidadMontoTotal
            // 
            this.lblCantidadMontoTotal.AutoSize = true;
            this.lblCantidadMontoTotal.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Bold);
            this.lblCantidadMontoTotal.ForeColor = System.Drawing.Color.White;
            this.lblCantidadMontoTotal.Location = new System.Drawing.Point(69, 392);
            this.lblCantidadMontoTotal.Name = "lblCantidadMontoTotal";
            this.lblCantidadMontoTotal.Size = new System.Drawing.Size(38, 28);
            this.lblCantidadMontoTotal.TabIndex = 18;
            this.lblCantidadMontoTotal.Text = "$0";
            // 
            // lblDireccion
            // 
            this.lblDireccion.AutoSize = true;
            this.lblDireccion.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDireccion.ForeColor = System.Drawing.Color.White;
            this.lblDireccion.Location = new System.Drawing.Point(12, 393);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(0, 22);
            this.lblDireccion.TabIndex = 17;
            // 
            // lblMontoTotal
            // 
            this.lblMontoTotal.AutoSize = true;
            this.lblMontoTotal.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lblMontoTotal.ForeColor = System.Drawing.Color.White;
            this.lblMontoTotal.Location = new System.Drawing.Point(12, 347);
            this.lblMontoTotal.Name = "lblMontoTotal";
            this.lblMontoTotal.Size = new System.Drawing.Size(169, 30);
            this.lblMontoTotal.TabIndex = 16;
            this.lblMontoTotal.Text = "Monto Total: ";
            // 
            // lblTelefono
            // 
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefono.ForeColor = System.Drawing.Color.White;
            this.lblTelefono.Location = new System.Drawing.Point(12, 315);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(0, 22);
            this.lblTelefono.TabIndex = 15;
            // 
            // lblTextoNombre
            // 
            this.lblTextoNombre.AutoSize = true;
            this.lblTextoNombre.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lblTextoNombre.ForeColor = System.Drawing.Color.White;
            this.lblTextoNombre.Location = new System.Drawing.Point(57, 181);
            this.lblTextoNombre.Name = "lblTextoNombre";
            this.lblTextoNombre.Size = new System.Drawing.Size(37, 30);
            this.lblTextoNombre.TabIndex = 14;
            this.lblTextoNombre.Text = "---";
            // 
            // lblEdad
            // 
            this.lblEdad.AutoSize = true;
            this.lblEdad.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdad.ForeColor = System.Drawing.Color.White;
            this.lblEdad.Location = new System.Drawing.Point(12, 237);
            this.lblEdad.Name = "lblEdad";
            this.lblEdad.Size = new System.Drawing.Size(0, 22);
            this.lblEdad.TabIndex = 13;
            // 
            // lblOrden
            // 
            this.lblOrden.AutoSize = true;
            this.lblOrden.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lblOrden.ForeColor = System.Drawing.Color.White;
            this.lblOrden.Location = new System.Drawing.Point(20, 50);
            this.lblOrden.Name = "lblOrden";
            this.lblOrden.Size = new System.Drawing.Size(126, 30);
            this.lblOrden.TabIndex = 12;
            this.lblOrden.Text = "Orden #: ";
            // 
            // lblSaludo
            // 
            this.lblSaludo.AutoSize = true;
            this.lblSaludo.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaludo.ForeColor = System.Drawing.Color.White;
            this.lblSaludo.Location = new System.Drawing.Point(12, 159);
            this.lblSaludo.Name = "lblSaludo";
            this.lblSaludo.Size = new System.Drawing.Size(0, 22);
            this.lblSaludo.TabIndex = 11;
            // 
            // lblNoOrden
            // 
            this.lblNoOrden.AutoSize = true;
            this.lblNoOrden.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lblNoOrden.ForeColor = System.Drawing.Color.White;
            this.lblNoOrden.Location = new System.Drawing.Point(161, 50);
            this.lblNoOrden.Name = "lblNoOrden";
            this.lblNoOrden.Size = new System.Drawing.Size(26, 30);
            this.lblNoOrden.TabIndex = 19;
            this.lblNoOrden.Text = "0";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lblNombre.ForeColor = System.Drawing.Color.White;
            this.lblNombre.Location = new System.Drawing.Point(21, 135);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(117, 30);
            this.lblNombre.TabIndex = 20;
            this.lblNombre.Text = "Nombre:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(323, 57);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(465, 330);
            this.dataGridView1.TabIndex = 7;
            // 
            // btn_Cancelar
            // 
            this.btn_Cancelar.BackColor = System.Drawing.Color.Red;
            this.btn_Cancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Cancelar.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cancelar.ForeColor = System.Drawing.Color.White;
            this.btn_Cancelar.Location = new System.Drawing.Point(568, 393);
            this.btn_Cancelar.Name = "btn_Cancelar";
            this.btn_Cancelar.Size = new System.Drawing.Size(220, 45);
            this.btn_Cancelar.TabIndex = 8;
            this.btn_Cancelar.Text = "Cancelar Orden";
            this.btn_Cancelar.UseVisualStyleBackColor = false;
            // 
            // lblEstatus
            // 
            this.lblEstatus.AutoSize = true;
            this.lblEstatus.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lblEstatus.ForeColor = System.Drawing.Color.White;
            this.lblEstatus.Location = new System.Drawing.Point(21, 237);
            this.lblEstatus.Name = "lblEstatus";
            this.lblEstatus.Size = new System.Drawing.Size(107, 30);
            this.lblEstatus.TabIndex = 21;
            this.lblEstatus.Text = "Estatus: ";
            // 
            // lblTipoEstatus
            // 
            this.lblTipoEstatus.AutoSize = true;
            this.lblTipoEstatus.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lblTipoEstatus.ForeColor = System.Drawing.Color.White;
            this.lblTipoEstatus.Location = new System.Drawing.Point(57, 286);
            this.lblTipoEstatus.Name = "lblTipoEstatus";
            this.lblTipoEstatus.Size = new System.Drawing.Size(37, 30);
            this.lblTipoEstatus.TabIndex = 22;
            this.lblTipoEstatus.Text = "---";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(425, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 30);
            this.label1.TabIndex = 23;
            this.label1.Text = "Detalles de la Orden";
            // 
            // btn_Cerrar
            // 
            this.btn_Cerrar.BackColor = System.Drawing.Color.Transparent;
            this.btn_Cerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Cerrar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_Cerrar.FlatAppearance.BorderSize = 0;
            this.btn_Cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Cerrar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cerrar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_Cerrar.Location = new System.Drawing.Point(770, 13);
            this.btn_Cerrar.Name = "btn_Cerrar";
            this.btn_Cerrar.Size = new System.Drawing.Size(30, 34);
            this.btn_Cerrar.TabIndex = 24;
            this.btn_Cerrar.Text = "X";
            this.btn_Cerrar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Cerrar.UseVisualStyleBackColor = false;
            this.btn_Cerrar.Click += new System.EventHandler(this.btn_Cerrar_Click);
            // 
            // PedidoDetalle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_Cerrar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_Cancelar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pan_head);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PedidoDetalle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PedidoDetalle";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pan_head;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCantidadMontoTotal;
        private System.Windows.Forms.Label lblDireccion;
        private System.Windows.Forms.Label lblMontoTotal;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.Label lblTextoNombre;
        private System.Windows.Forms.Label lblEdad;
        private System.Windows.Forms.Label lblOrden;
        private System.Windows.Forms.Label lblSaludo;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblNoOrden;
        private System.Windows.Forms.Label lblTipoEstatus;
        private System.Windows.Forms.Label lblEstatus;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Cancelar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Cerrar;
    }
}