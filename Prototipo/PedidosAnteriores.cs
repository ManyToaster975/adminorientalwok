﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Prototipo
{
    public partial class PedidosAnteriores : Form
    {
        private int idCliente;
        private Prototipo.Menu m;
        public static string cnn = ("Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + @"\BD.mdb");
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public PedidosAnteriores(Menu m,int idCliente)
        {
            this.InitializeComponent();
            this.idCliente = idCliente;
            this.m = m;
        }
        private void btn_Cerrar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Desea volver al menu de usuario?", "Advertencia", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                m.Show();
                this.Close();
            }
        }

        private void PedidosAnteriores_Load(object sender, EventArgs e)
        {
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            DataTable dataTable = new DataTable();
            new OleDbDataAdapter("SELECT IdOrden FROM Orden WHERE IdCliente=" + this.idCliente.ToString(), cnn).Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                for (int i = 0; i <= (dataTable.Rows.Count - 1); i++)
                {
                    Pedido pedido = new Pedido(int.Parse(dataTable.Rows[i]["IdOrden"].ToString()));
                    this.pnOrdenes.Controls.Add(pedido);
                    int x = 0xbf * i;
                    int y = 12;
                    pedido.Location = new Point(x, y);
                }
            }
        }

        private void PedidosAnteriores_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
