﻿namespace Prototipo
{
    partial class Ordenar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ordenar));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_confirmacion = new System.Windows.Forms.Label();
            this.lbl_comida = new System.Windows.Forms.Label();
            this.lbl_clientes = new System.Windows.Forms.Label();
            this.pan_Marca = new System.Windows.Forms.Panel();
            this.pan_head = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Cerrar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.Fuerte = new System.Windows.Forms.Button();
            this.Postre = new System.Windows.Forms.Button();
            this.Entrada = new System.Windows.Forms.Button();
            this.Bebida = new System.Windows.Forms.Button();
            this.panelArticulos = new System.Windows.Forms.Panel();
            this.lblNombreP = new System.Windows.Forms.Label();
            this.bt_adelante = new System.Windows.Forms.Button();
            this.bt_atras = new System.Windows.Forms.Button();
            this.pn_Confirmar = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.lblMontoTotal = new System.Windows.Forms.Label();
            this.btnConf = new System.Windows.Forms.Button();
            this.lsb_ConfirmOrden = new System.Windows.Forms.ListBox();
            this.lsb_ConfirmComensal = new System.Windows.Forms.ListBox();
            this.pn_regComensales = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnFyH = new System.Windows.Forms.Panel();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lsb_comensales = new System.Windows.Forms.ListBox();
            this.txt_nombre = new System.Windows.Forms.TextBox();
            this.bt_agregar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_edad = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnEliminarItem = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lblMonto = new System.Windows.Forms.Label();
            this.lsbCarrito = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panelMenu.SuspendLayout();
            this.panel10.SuspendLayout();
            this.pn_Confirmar.SuspendLayout();
            this.pn_regComensales.SuspendLayout();
            this.pnFyH.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel1.Controls.Add(this.lbl_confirmacion);
            this.panel1.Controls.Add(this.lbl_comida);
            this.panel1.Controls.Add(this.lbl_clientes);
            this.panel1.Controls.Add(this.pan_Marca);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(173, 592);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // lbl_confirmacion
            // 
            this.lbl_confirmacion.AutoSize = true;
            this.lbl_confirmacion.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.lbl_confirmacion.ForeColor = System.Drawing.Color.White;
            this.lbl_confirmacion.Location = new System.Drawing.Point(12, 439);
            this.lbl_confirmacion.Name = "lbl_confirmacion";
            this.lbl_confirmacion.Size = new System.Drawing.Size(165, 69);
            this.lbl_confirmacion.TabIndex = 13;
            this.lbl_confirmacion.Text = "☑ Confirmación \r\nde la\r\norden";
            // 
            // lbl_comida
            // 
            this.lbl_comida.AutoSize = true;
            this.lbl_comida.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.lbl_comida.ForeColor = System.Drawing.Color.White;
            this.lbl_comida.Location = new System.Drawing.Point(19, 310);
            this.lbl_comida.Name = "lbl_comida";
            this.lbl_comida.Size = new System.Drawing.Size(114, 69);
            this.lbl_comida.TabIndex = 12;
            this.lbl_comida.Text = "☑ Comida \r\npor \r\npersona";
            // 
            // lbl_clientes
            // 
            this.lbl_clientes.AutoSize = true;
            this.lbl_clientes.Font = new System.Drawing.Font("Century Gothic", 15F);
            this.lbl_clientes.ForeColor = System.Drawing.Color.White;
            this.lbl_clientes.Location = new System.Drawing.Point(20, 150);
            this.lbl_clientes.Name = "lbl_clientes";
            this.lbl_clientes.Size = new System.Drawing.Size(113, 69);
            this.lbl_clientes.TabIndex = 10;
            this.lbl_clientes.Text = "☑ Quiénes \r\nvan a \r\ncomer";
            // 
            // pan_Marca
            // 
            this.pan_Marca.BackColor = System.Drawing.Color.Red;
            this.pan_Marca.Location = new System.Drawing.Point(12, 153);
            this.pan_Marca.Name = "pan_Marca";
            this.pan_Marca.Size = new System.Drawing.Size(10, 66);
            this.pan_Marca.TabIndex = 1;
            // 
            // pan_head
            // 
            this.pan_head.BackColor = System.Drawing.Color.Red;
            this.pan_head.Dock = System.Windows.Forms.DockStyle.Top;
            this.pan_head.Location = new System.Drawing.Point(0, 0);
            this.pan_head.Margin = new System.Windows.Forms.Padding(0);
            this.pan_head.Name = "pan_head";
            this.pan_head.Size = new System.Drawing.Size(1288, 10);
            this.pan_head.TabIndex = 1;
            this.pan_head.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BackColor = System.Drawing.SystemColors.Control;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.btn_Cerrar);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Location = new System.Drawing.Point(179, 9);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(854, 149);
            this.panel3.TabIndex = 1;
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(280, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 40);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ordenar en línea";
            // 
            // btn_Cerrar
            // 
            this.btn_Cerrar.BackColor = System.Drawing.Color.Transparent;
            this.btn_Cerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Cerrar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_Cerrar.FlatAppearance.BorderSize = 0;
            this.btn_Cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Cerrar.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Cerrar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_Cerrar.Location = new System.Drawing.Point(822, 3);
            this.btn_Cerrar.Name = "btn_Cerrar";
            this.btn_Cerrar.Size = new System.Drawing.Size(30, 34);
            this.btn_Cerrar.TabIndex = 7;
            this.btn_Cerrar.Text = "X";
            this.btn_Cerrar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Cerrar.UseVisualStyleBackColor = false;
            this.btn_Cerrar.Click += new System.EventHandler(this.btn_Cerrar_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(24, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(98, 140);
            this.panel2.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Prototipo.Properties.Resources.Imagen2;
            this.pictureBox2.Location = new System.Drawing.Point(16, 79);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(66, 45);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Prototipo.Properties.Resources.Imagen1;
            this.pictureBox1.Location = new System.Drawing.Point(16, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(77, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 32);
            this.label2.TabIndex = 9;
            this.label2.Text = "Carrito";
            // 
            // panel4
            // 
            this.panel4.AutoScroll = true;
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.Controls.Add(this.panelMenu);
            this.panel4.Controls.Add(this.bt_adelante);
            this.panel4.Controls.Add(this.bt_atras);
            this.panel4.Controls.Add(this.pn_Confirmar);
            this.panel4.Controls.Add(this.pn_regComensales);
            this.panel4.Location = new System.Drawing.Point(179, 160);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(854, 438);
            this.panel4.TabIndex = 9;
            this.panel4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.panel10);
            this.panelMenu.Controls.Add(this.panelArticulos);
            this.panelMenu.Controls.Add(this.lblNombreP);
            this.panelMenu.Location = new System.Drawing.Point(865, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(813, 356);
            this.panelMenu.TabIndex = 19;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.panel10.Controls.Add(this.Fuerte);
            this.panel10.Controls.Add(this.Postre);
            this.panel10.Controls.Add(this.Entrada);
            this.panel10.Controls.Add(this.Bebida);
            this.panel10.Location = new System.Drawing.Point(18, 323);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(792, 33);
            this.panel10.TabIndex = 16;
            // 
            // Fuerte
            // 
            this.Fuerte.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.Fuerte.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Fuerte.FlatAppearance.BorderSize = 0;
            this.Fuerte.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Fuerte.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fuerte.ForeColor = System.Drawing.Color.Tomato;
            this.Fuerte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Fuerte.Location = new System.Drawing.Point(245, 2);
            this.Fuerte.Name = "Fuerte";
            this.Fuerte.Size = new System.Drawing.Size(123, 28);
            this.Fuerte.TabIndex = 12;
            this.Fuerte.Text = "Platos Fuertes";
            this.Fuerte.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Fuerte.UseVisualStyleBackColor = false;
            this.Fuerte.Click += new System.EventHandler(this.cambiarMenu);
            // 
            // Postre
            // 
            this.Postre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.Postre.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Postre.FlatAppearance.BorderSize = 0;
            this.Postre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Postre.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Postre.ForeColor = System.Drawing.Color.Tomato;
            this.Postre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Postre.Location = new System.Drawing.Point(499, 3);
            this.Postre.Name = "Postre";
            this.Postre.Size = new System.Drawing.Size(69, 28);
            this.Postre.TabIndex = 13;
            this.Postre.Text = "Postres";
            this.Postre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Postre.UseVisualStyleBackColor = false;
            this.Postre.Click += new System.EventHandler(this.cambiarMenu);
            // 
            // Entrada
            // 
            this.Entrada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.Entrada.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Entrada.FlatAppearance.BorderSize = 0;
            this.Entrada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Entrada.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Entrada.ForeColor = System.Drawing.Color.Tomato;
            this.Entrada.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Entrada.Location = new System.Drawing.Point(3, 2);
            this.Entrada.Name = "Entrada";
            this.Entrada.Size = new System.Drawing.Size(82, 28);
            this.Entrada.TabIndex = 11;
            this.Entrada.Text = "Entradas";
            this.Entrada.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Entrada.UseVisualStyleBackColor = false;
            this.Entrada.Click += new System.EventHandler(this.cambiarMenu);
            // 
            // Bebida
            // 
            this.Bebida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.Bebida.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Bebida.FlatAppearance.BorderSize = 0;
            this.Bebida.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Bebida.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Bebida.ForeColor = System.Drawing.Color.Tomato;
            this.Bebida.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Bebida.Location = new System.Drawing.Point(707, 2);
            this.Bebida.Name = "Bebida";
            this.Bebida.Size = new System.Drawing.Size(82, 28);
            this.Bebida.TabIndex = 14;
            this.Bebida.Text = "Bebidas";
            this.Bebida.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Bebida.UseVisualStyleBackColor = false;
            this.Bebida.Click += new System.EventHandler(this.cambiarMenu);
            // 
            // panelArticulos
            // 
            this.panelArticulos.AutoScroll = true;
            this.panelArticulos.Location = new System.Drawing.Point(18, 33);
            this.panelArticulos.Name = "panelArticulos";
            this.panelArticulos.Size = new System.Drawing.Size(813, 286);
            this.panelArticulos.TabIndex = 15;
            // 
            // lblNombreP
            // 
            this.lblNombreP.AutoSize = true;
            this.lblNombreP.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold);
            this.lblNombreP.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblNombreP.Location = new System.Drawing.Point(331, 3);
            this.lblNombreP.Name = "lblNombreP";
            this.lblNombreP.Size = new System.Drawing.Size(152, 19);
            this.lblNombreP.TabIndex = 10;
            this.lblNombreP.Text = "Qué va a comer....";
            // 
            // bt_adelante
            // 
            this.bt_adelante.BackgroundImage = global::Prototipo.Properties.Resources.next__1_;
            this.bt_adelante.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_adelante.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_adelante.FlatAppearance.BorderSize = 0;
            this.bt_adelante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_adelante.Location = new System.Drawing.Point(767, 359);
            this.bt_adelante.Margin = new System.Windows.Forms.Padding(0);
            this.bt_adelante.Name = "bt_adelante";
            this.bt_adelante.Size = new System.Drawing.Size(50, 50);
            this.bt_adelante.TabIndex = 0;
            this.bt_adelante.UseVisualStyleBackColor = true;
            this.bt_adelante.Click += new System.EventHandler(this.bt_adelante_Click);
            // 
            // bt_atras
            // 
            this.bt_atras.BackgroundImage = global::Prototipo.Properties.Resources.back;
            this.bt_atras.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_atras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_atras.FlatAppearance.BorderSize = 0;
            this.bt_atras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_atras.Location = new System.Drawing.Point(24, 359);
            this.bt_atras.Margin = new System.Windows.Forms.Padding(0);
            this.bt_atras.Name = "bt_atras";
            this.bt_atras.Size = new System.Drawing.Size(50, 50);
            this.bt_atras.TabIndex = 18;
            this.bt_atras.UseVisualStyleBackColor = true;
            this.bt_atras.Visible = false;
            this.bt_atras.Click += new System.EventHandler(this.bt_atras_Click);
            // 
            // pn_Confirmar
            // 
            this.pn_Confirmar.Controls.Add(this.label9);
            this.pn_Confirmar.Controls.Add(this.lblMontoTotal);
            this.pn_Confirmar.Controls.Add(this.btnConf);
            this.pn_Confirmar.Controls.Add(this.lsb_ConfirmOrden);
            this.pn_Confirmar.Controls.Add(this.lsb_ConfirmComensal);
            this.pn_Confirmar.Location = new System.Drawing.Point(24, 412);
            this.pn_Confirmar.Name = "pn_Confirmar";
            this.pn_Confirmar.Size = new System.Drawing.Size(796, 356);
            this.pn_Confirmar.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(551, 281);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 24);
            this.label9.TabIndex = 13;
            this.label9.Text = "Total:";
            // 
            // lblMontoTotal
            // 
            this.lblMontoTotal.AutoSize = true;
            this.lblMontoTotal.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMontoTotal.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblMontoTotal.Location = new System.Drawing.Point(620, 281);
            this.lblMontoTotal.Name = "lblMontoTotal";
            this.lblMontoTotal.Size = new System.Drawing.Size(63, 24);
            this.lblMontoTotal.TabIndex = 13;
            this.lblMontoTotal.Text = "$0,00";
            // 
            // btnConf
            // 
            this.btnConf.BackColor = System.Drawing.Color.Green;
            this.btnConf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConf.FlatAppearance.BorderSize = 0;
            this.btnConf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConf.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.btnConf.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnConf.Image = global::Prototipo.Properties.Resources.menu1;
            this.btnConf.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConf.Location = new System.Drawing.Point(601, 319);
            this.btnConf.Name = "btnConf";
            this.btnConf.Size = new System.Drawing.Size(209, 34);
            this.btnConf.TabIndex = 25;
            this.btnConf.Text = "           Confirmar Orden";
            this.btnConf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConf.UseVisualStyleBackColor = false;
            this.btnConf.Click += new System.EventHandler(this.btnConf_Click);
            // 
            // lsb_ConfirmOrden
            // 
            this.lsb_ConfirmOrden.BackColor = System.Drawing.SystemColors.Control;
            this.lsb_ConfirmOrden.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lsb_ConfirmOrden.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsb_ConfirmOrden.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lsb_ConfirmOrden.FormattingEnabled = true;
            this.lsb_ConfirmOrden.ItemHeight = 23;
            this.lsb_ConfirmOrden.Location = new System.Drawing.Point(21, 3);
            this.lsb_ConfirmOrden.Name = "lsb_ConfirmOrden";
            this.lsb_ConfirmOrden.Size = new System.Drawing.Size(380, 230);
            this.lsb_ConfirmOrden.TabIndex = 17;
            // 
            // lsb_ConfirmComensal
            // 
            this.lsb_ConfirmComensal.BackColor = System.Drawing.SystemColors.Control;
            this.lsb_ConfirmComensal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lsb_ConfirmComensal.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsb_ConfirmComensal.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lsb_ConfirmComensal.FormattingEnabled = true;
            this.lsb_ConfirmComensal.ItemHeight = 23;
            this.lsb_ConfirmComensal.Location = new System.Drawing.Point(444, 3);
            this.lsb_ConfirmComensal.Name = "lsb_ConfirmComensal";
            this.lsb_ConfirmComensal.Size = new System.Drawing.Size(373, 253);
            this.lsb_ConfirmComensal.TabIndex = 16;
            // 
            // pn_regComensales
            // 
            this.pn_regComensales.Controls.Add(this.label10);
            this.pn_regComensales.Controls.Add(this.label8);
            this.pn_regComensales.Controls.Add(this.panel6);
            this.pn_regComensales.Controls.Add(this.pnFyH);
            this.pn_regComensales.Controls.Add(this.btnEliminar);
            this.pn_regComensales.Controls.Add(this.label3);
            this.pn_regComensales.Controls.Add(this.lsb_comensales);
            this.pn_regComensales.Controls.Add(this.txt_nombre);
            this.pn_regComensales.Controls.Add(this.bt_agregar);
            this.pn_regComensales.Controls.Add(this.panel5);
            this.pn_regComensales.Controls.Add(this.label4);
            this.pn_regComensales.Controls.Add(this.txt_edad);
            this.pn_regComensales.Location = new System.Drawing.Point(0, 0);
            this.pn_regComensales.Name = "pn_regComensales";
            this.pn_regComensales.Size = new System.Drawing.Size(833, 356);
            this.pn_regComensales.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(131, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(161, 23);
            this.label10.TabIndex = 21;
            this.label10.Text = "Acompañantes";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(621, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 23);
            this.label8.TabIndex = 20;
            this.label8.Text = "Mesa";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Red;
            this.panel6.Location = new System.Drawing.Point(120, 138);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(143, 3);
            this.panel6.TabIndex = 3;
            // 
            // pnFyH
            // 
            this.pnFyH.Controls.Add(this.dateTimePicker2);
            this.pnFyH.Controls.Add(this.dateTimePicker1);
            this.pnFyH.Controls.Add(this.panel8);
            this.pnFyH.Controls.Add(this.label5);
            this.pnFyH.Controls.Add(this.label6);
            this.pnFyH.Controls.Add(this.panel9);
            this.pnFyH.Location = new System.Drawing.Point(178, 232);
            this.pnFyH.Name = "pnFyH";
            this.pnFyH.Size = new System.Drawing.Size(266, 108);
            this.pnFyH.TabIndex = 19;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "hh:mm tt";
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(109, 64);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowUpDown = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(146, 32);
            this.dateTimePicker2.TabIndex = 18;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(109, 3);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(146, 32);
            this.dateTimePicker1.TabIndex = 17;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Red;
            this.panel8.Location = new System.Drawing.Point(110, 36);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(143, 3);
            this.panel8.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(3, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 23);
            this.label5.TabIndex = 10;
            this.label5.Text = "Fecha: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(3, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 23);
            this.label6.TabIndex = 13;
            this.label6.Text = "Hora: ";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Red;
            this.panel9.Location = new System.Drawing.Point(110, 96);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(143, 3);
            this.panel9.TabIndex = 12;
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackgroundImage = global::Prototipo.Properties.Resources.cancel;
            this.btnEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminar.FlatAppearance.BorderSize = 0;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminar.Location = new System.Drawing.Point(406, 118);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(0);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(38, 38);
            this.btnEliminar.TabIndex = 16;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(34, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 23);
            this.label3.TabIndex = 10;
            this.label3.Text = "Nombre:";
            // 
            // lsb_comensales
            // 
            this.lsb_comensales.BackColor = System.Drawing.SystemColors.Control;
            this.lsb_comensales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lsb_comensales.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsb_comensales.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lsb_comensales.FormattingEnabled = true;
            this.lsb_comensales.ItemHeight = 23;
            this.lsb_comensales.Location = new System.Drawing.Point(486, 52);
            this.lsb_comensales.Name = "lsb_comensales";
            this.lsb_comensales.Size = new System.Drawing.Size(344, 278);
            this.lsb_comensales.TabIndex = 15;
            // 
            // txt_nombre
            // 
            this.txt_nombre.BackColor = System.Drawing.SystemColors.Control;
            this.txt_nombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_nombre.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Italic);
            this.txt_nombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.txt_nombre.Location = new System.Drawing.Point(135, 76);
            this.txt_nombre.Name = "txt_nombre";
            this.txt_nombre.Size = new System.Drawing.Size(227, 25);
            this.txt_nombre.TabIndex = 1;
            this.txt_nombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_nombre_KeyPress);
            // 
            // bt_agregar
            // 
            this.bt_agregar.BackgroundImage = global::Prototipo.Properties.Resources.plus__2_;
            this.bt_agregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bt_agregar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bt_agregar.FlatAppearance.BorderSize = 0;
            this.bt_agregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_agregar.Location = new System.Drawing.Point(406, 58);
            this.bt_agregar.Margin = new System.Windows.Forms.Padding(0);
            this.bt_agregar.Name = "bt_agregar";
            this.bt_agregar.Size = new System.Drawing.Size(38, 38);
            this.bt_agregar.TabIndex = 14;
            this.bt_agregar.UseVisualStyleBackColor = true;
            this.bt_agregar.Click += new System.EventHandler(this.bt_agregar_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Red;
            this.panel5.Location = new System.Drawing.Point(120, 103);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(242, 2);
            this.panel5.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(48, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 23);
            this.label4.TabIndex = 13;
            this.label4.Text = "Edad:";
            // 
            // txt_edad
            // 
            this.txt_edad.BackColor = System.Drawing.SystemColors.Control;
            this.txt_edad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_edad.Font = new System.Drawing.Font("Century Gothic", 15F, System.Drawing.FontStyle.Italic);
            this.txt_edad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.txt_edad.Location = new System.Drawing.Point(162, 111);
            this.txt_edad.MaxLength = 2;
            this.txt_edad.Name = "txt_edad";
            this.txt_edad.Size = new System.Drawing.Size(51, 25);
            this.txt_edad.TabIndex = 11;
            this.txt_edad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_edad_KeyPress);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this.btnEliminarItem);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.lblMonto);
            this.panel7.Controls.Add(this.lsbCarrito);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Location = new System.Drawing.Point(1035, 9);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(249, 589);
            this.panel7.TabIndex = 10;
            this.panel7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            // 
            // btnEliminarItem
            // 
            this.btnEliminarItem.BackgroundImage = global::Prototipo.Properties.Resources.cancel;
            this.btnEliminarItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEliminarItem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarItem.FlatAppearance.BorderSize = 0;
            this.btnEliminarItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarItem.Location = new System.Drawing.Point(107, 451);
            this.btnEliminarItem.Margin = new System.Windows.Forms.Padding(0);
            this.btnEliminarItem.Name = "btnEliminarItem";
            this.btnEliminarItem.Size = new System.Drawing.Size(39, 40);
            this.btnEliminarItem.TabIndex = 20;
            this.btnEliminarItem.UseVisualStyleBackColor = true;
            this.btnEliminarItem.Click += new System.EventHandler(this.btnEliminarItem_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Location = new System.Drawing.Point(45, 510);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 24);
            this.label7.TabIndex = 12;
            this.label7.Text = "Total:";
            // 
            // lblMonto
            // 
            this.lblMonto.AutoSize = true;
            this.lblMonto.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMonto.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblMonto.Location = new System.Drawing.Point(140, 510);
            this.lblMonto.Name = "lblMonto";
            this.lblMonto.Size = new System.Drawing.Size(63, 24);
            this.lblMonto.TabIndex = 11;
            this.lblMonto.Text = "$0,00";
            // 
            // lsbCarrito
            // 
            this.lsbCarrito.BackColor = System.Drawing.SystemColors.Control;
            this.lsbCarrito.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lsbCarrito.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsbCarrito.FormattingEnabled = true;
            this.lsbCarrito.ItemHeight = 21;
            this.lsbCarrito.Location = new System.Drawing.Point(9, 152);
            this.lsbCarrito.Name = "lsbCarrito";
            this.lsbCarrito.Size = new System.Drawing.Size(228, 296);
            this.lsbCarrito.TabIndex = 10;
            // 
            // Ordenar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.CancelButton = this.btn_Cerrar;
            this.ClientSize = new System.Drawing.Size(1288, 602);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.pan_head);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel3);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Ordenar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Main_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Main_MouseDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.pn_Confirmar.ResumeLayout(false);
            this.pn_Confirmar.PerformLayout();
            this.pn_regComensales.ResumeLayout(false);
            this.pn_regComensales.PerformLayout();
            this.pnFyH.ResumeLayout(false);
            this.pnFyH.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pan_head;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel pan_Marca;
        private System.Windows.Forms.Button btn_Cerrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bt_adelante;
        private System.Windows.Forms.Label lbl_clientes;
        private System.Windows.Forms.Label lbl_confirmacion;
        private System.Windows.Forms.Label lbl_comida;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_edad;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txt_nombre;
        private System.Windows.Forms.Button bt_agregar;
        private System.Windows.Forms.ListBox lsb_comensales;
        private System.Windows.Forms.Panel pn_regComensales;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel pn_Confirmar;
        private System.Windows.Forms.ListBox lsb_ConfirmComensal;
        private System.Windows.Forms.ListBox lsb_ConfirmOrden;
        private System.Windows.Forms.Button bt_atras;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Label lblNombreP;
        private System.Windows.Forms.Button Bebida;
        private System.Windows.Forms.Button Postre;
        private System.Windows.Forms.Button Fuerte;
        private System.Windows.Forms.Button Entrada;
        private System.Windows.Forms.Panel panelArticulos;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ListBox lsbCarrito;
        private System.Windows.Forms.Button btnConf;
        private System.Windows.Forms.Panel pnFyH;
        private System.Windows.Forms.Label lblMonto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblMontoTotal;
        private System.Windows.Forms.Button btnEliminarItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label10;
    }
}