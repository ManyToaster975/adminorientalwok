﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public class Item
    {
        private String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        public int artCve;
        public int cantidad;
        public string nombre;
        public double monto;
        public Item(int artCve, int cantidad, string nombre, int costo)
        {
            this.artCve = artCve;
            this.cantidad = cantidad;
            this.nombre = nombre;
            monto = costo * cantidad;
        }

        public void guardarDetalle(int idPersona){
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Rel_Per_Art WHERE IdRelacionPerArt=0";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            DataRow dr = dt.NewRow();
            dr["IdPersona"] = idPersona;
            dr["idArticulo"] = artCve;
            dr["Cantidad"] = cantidad;
            dr["Monto"] = monto;
            dt.Rows.Add(dr);
            OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
            da.InsertCommand = cb.GetInsertCommand();
            da.Update(dt);
            dt.AcceptChanges();
        }
    }
}
