﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public partial class Ordenar : Form
    {
        public Ordenar(Menu m, int idCliente)
        {
            InitializeComponent();
            x = m;
            this.idCliente = idCliente;
        }
        public static String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        List<string> orden = new List<string>();
        int idCliente;
        Menu x;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void btn_Cerrar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Desea cancelar su orden?", "Advertencia", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                x.Show();
                this.Close();
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            OrdenGlobal.idCliente = idCliente;
            OrdenGlobal.ordenGlobal = new List<Persona>();
            cargarPantallaComensales();
            cargarMenu("Entrada");
        }

        private void Main_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void bt_agregar_Click(object sender, EventArgs e)
        {
            if (txt_nombre.Text == "" || txt_edad.Text == "")
            {
                MessageBox.Show("Favor de rellenar todos los datos");
            }
            else
            {
                lsb_comensales.Items.Add(txt_nombre.Text + " " + txt_edad.Text + " años");
                OrdenGlobal.ordenGlobal.Add(new Persona(txt_nombre.Text, int.Parse(txt_edad.Text)));
                txt_nombre.Text = "";
                txt_edad.Text = "";
            }
        }

        private void cargarPantallaComensales()
        {
            this.ClientSize = new System.Drawing.Size(1034, 602);
            OrdenGlobal.currentPerson = -1;
            pn_regComensales.Visible = true;
            pn_regComensales.Location = new Point(0, 0);
            panelMenu.Visible = false;
            pn_Confirmar.Visible = false;
            bt_atras.Visible = false;
            bt_adelante.Visible = true;
            pan_Marca.Location = new Point(0, lbl_clientes.Location.Y);
            lbl_clientes.ForeColor = Color.White;
            pn_regComensales.Controls.Add(pnFyH);
            pnFyH.Location = new Point(40, 190);
        }
        private void cargarPantallaMenu(int p)
        {
            if (OrdenGlobal.ordenGlobal.Count > 0)
            {
                this.ClientSize = new System.Drawing.Size(1287, 602);
                pn_regComensales.Visible = false;
                panelMenu.Visible = true;
                panelMenu.Location = new Point(0, 0);
                pn_Confirmar.Visible = false;
                bt_adelante.Visible = true;
                bt_atras.Visible = true;
                pan_Marca.Location = new Point(0, lbl_comida.Location.Y);
                lbl_clientes.ForeColor = Color.LimeGreen;
                lbl_comida.ForeColor = Color.White;
                lblNombreP.Text = "¿Qué va a comer " + OrdenGlobal.ordenGlobal[p].nombre + "?";
                cargarCarrito();
            }
            else
            {
                MessageBox.Show("Añada al menos un cliente");
            }
        }
        private void cargarPantallaConfirmacion()
        {
            if (OrdenGlobal.ordenGlobal.Count > 0)
            {
                this.ClientSize = new System.Drawing.Size(1034, 602);
                pn_regComensales.Visible = false;
                panelMenu.Visible = false;
                pn_Confirmar.Visible = true;
                pn_Confirmar.Location = new Point(0, 0);
                bt_adelante.Visible = false;
                bt_atras.Visible = true;
                pan_Marca.Location = new Point(0, lbl_confirmacion.Location.Y);
                lbl_comida.ForeColor = Color.LimeGreen;
                llenarConfirmacion();
                pn_Confirmar.Controls.Add(pnFyH);
                pnFyH.Location = new Point(0, 244);
            }
            else
            {
                MessageBox.Show("Añada al menos un cliente");
            }
        }

        private void bt_adelante_Click(object sender, EventArgs e)
        {
            if (lsb_comensales.Items.Count > 0)
            {
                if (panelMenu.Visible)
                {
                    if (lsbCarrito.Items.Count > 0)
                    {
                        OrdenGlobal.currentPerson++;
                        if (OrdenGlobal.currentPerson <= OrdenGlobal.ordenGlobal.Count - 1)
                            cargarPantallaMenu(OrdenGlobal.currentPerson);
                        else
                        {
                            cargarPantallaConfirmacion();
                        }
                    }
                    else
                        MessageBox.Show("Debe elegir al menos un artículo");
                }
                else
                {
                    OrdenGlobal.currentPerson++;
                    if (OrdenGlobal.currentPerson <= OrdenGlobal.ordenGlobal.Count - 1)
                        cargarPantallaMenu(OrdenGlobal.currentPerson);
                    else
                    {
                        cargarPantallaConfirmacion();
                    }
                }
            }
            else
            {
                MessageBox.Show("Debe añadir al menos un comensal");
            }
        }

        private void llenarConfirmacion()
        {
            lsb_ConfirmComensal.Items.Clear();
            lsb_ConfirmOrden.Items.Clear();
            foreach (Persona p in OrdenGlobal.ordenGlobal)
            {
                lsb_ConfirmComensal.Items.Add("Nombre: " + p.nombre + " \nEdad: " + p.edad.ToString());
                foreach (Item item in p.orden)
                {
                    lsb_ConfirmOrden.Items.Add(item.nombre + " x " + item.cantidad.ToString() + "= " + item.monto.ToString());
                }
            }
            lblMontoTotal.Text = String.Format("${0:n}", OrdenGlobal.montoTotal());
        }

        public void cargarCarrito()
        {
            lsbCarrito.Items.Clear();
            foreach (Item item in OrdenGlobal.ordenGlobal[OrdenGlobal.currentPerson].orden)
            {
                lsbCarrito.Items.Add(item.nombre + " x " + item.cantidad.ToString());
            }
            lblMonto.Text = String.Format("${0:n}", OrdenGlobal.ordenGlobal[OrdenGlobal.currentPerson].monto());
        }

        private void bt_atras_Click(object sender, EventArgs e)
        {
            OrdenGlobal.currentPerson--;
            if (OrdenGlobal.currentPerson >= 0)
                cargarPantallaMenu(OrdenGlobal.currentPerson);
            else
            {
                cargarPantallaComensales();
            }
        }

        private void txt_edad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar)) return;
            if (Char.IsControl(e.KeyChar)) return;
            e.Handled = true;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int borrar = lsb_comensales.SelectedIndex;
            if (borrar == -1)
            {
                MessageBox.Show("Seleccione una persona de la lista");
                return;
            }
            DialogResult dr = MessageBox.Show("¿Está seguro que desea eliminar a esta persona de la orden?", "Advertencia", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                lsb_comensales.Items.RemoveAt(borrar);
                OrdenGlobal.ordenGlobal.RemoveAt(borrar);
            }
        }

        public void cargarMenu(String menu)
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT IdArticulo FROM Articulo WHERE Tipo='" + menu + "'";
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            panelArticulos.Controls.Clear();
            if (dt.Rows.Count > 0)
            {
                int factor = 0;
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    Articulo art = new Articulo(Int32.Parse(dt.Rows[i]["IdArticulo"].ToString()), this);
                    panelArticulos.Controls.Add(art);
                    int x = 0, y = 0;
                    if (i == 0)
                    {
                        x = 0; y = 0;
                    }
                    else if (i % 2 == 0)
                    {
                        x = 0;
                        y = 147 * (factor);
                    }
                    else
                    {
                        x = 400;
                        y = 147 * (factor);
                        factor++;
                    }
                    art.Location = new Point(x, y);
                }
            }
        }

        private void cambiarMenu(object sender, EventArgs e)
        {
            Button txt = (Button)sender;
            cargarMenu(txt.Name);
        }

        private void btnConf_Click(object sender, EventArgs e)
        {
            lbl_confirmacion.ForeColor = Color.LimeGreen;
            DialogResult dres = MessageBox.Show("La orden esta correcta?", "Confirmar", MessageBoxButtons.YesNo);
            if (dres == DialogResult.Yes)
            {
                OrdenGlobal.fecha = dateTimePicker1.Value.ToString("yyyy-MM-dd");
                OrdenGlobal.hora = dateTimePicker2.Value.ToString("hh:mm tt");
                OrdenGlobal.guardarOrden();
                MessageBox.Show("Pedido creado");
                OrdenGlobal.ordenGlobal.Clear();
                x.Show();
                this.Close();
            }
            else if(dres==DialogResult.No)
            {
                lbl_confirmacion.ForeColor = Color.White;
            }
            
        }

        private void btnEliminarItem_Click(object sender, EventArgs e)
        {
            int borrar = lsbCarrito.SelectedIndex;
            if (borrar == -1)
            {
                MessageBox.Show("Seleccione un artículo del carrito");
                return;
            }
            DialogResult dr = MessageBox.Show("¿Está seguro que desea eliminar este artículo de la orden?", "Advertencia", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                lsbCarrito.Items.RemoveAt(borrar);
                OrdenGlobal.ordenGlobal[OrdenGlobal.currentPerson].orden.RemoveAt(borrar);
            }
        }

        private void txt_nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo permite letras
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
