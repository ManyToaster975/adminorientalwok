﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Prototipo
{
    public partial class PedidoDetalle : Form
    {
        public PedidoDetalle(Menu menu, int idCliente)
        {
            InitializeComponent();
            x = menu;
        }

        Menu x;
        private void btn_Cerrar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Desea volver al menu de usuario?", "Advertencia", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                x.Show();
                this.Close();
            }
        }
    }
}
