﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public partial class Cliente : UserControl
    {
        public Cliente(int idUsuario, Menu m)
        {
            InitializeComponent();
            this.idUsuario = idUsuario;
            this.m = m;
        }
        public static String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        int idUsuario;
        Menu m;

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(txtNombre.Text=="")
            {
                MessageBox.Show("Favor de especificar nombre");
                return;
            }
            if (txtTelefono.Text == "")
            {
                MessageBox.Show("Favor de especificar telefono");
                return;
            }
            if (txtDireccion.Text == "")
            {
                MessageBox.Show("Favor de especificar direccion");
                return;
            }
            if (txtEdad.Text == "")
            {
                MessageBox.Show("Favor de especificar edad");
                return;
            }
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Cliente WHERE IdUsuario=" + idUsuario.ToString();
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
            if (dt.Rows.Count > 0)
            {
                dt.Rows[0]["Nombre"]=txtNombre.Text;
                dt.Rows[0]["Telefono"]=txtTelefono.Text;
                dt.Rows[0]["Direccion"]=txtDireccion.Text;
                dt.Rows[0]["Edad"] = txtEdad.Text;
                da.UpdateCommand = cb.GetUpdateCommand();
            }
            else
            {
                DataRow dr = dt.NewRow();
                dr["Nombre"] = txtNombre.Text;
                dr["Telefono"] = txtTelefono.Text;
                dr["Direccion"] = txtDireccion.Text;
                dr["Edad"] = txtEdad.Text;
                dr["IdUsuario"] = idUsuario;
                dt.Rows.Add(dr);
                da.InsertCommand = cb.GetInsertCommand();
            }
            da.Update(dt);
            dt.AcceptChanges();
            MessageBox.Show("Cambios guardados");
            m.cargarMenu();
            this.Dispose();
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char chr = e.KeyChar;
            if (!Char.IsDigit(chr) && chr != 8)
            {
                e.Handled = true;
            }
        }

        private void txtEdad_KeyPress(object sender, KeyPressEventArgs e)
        {
            Char chr = e.KeyChar;
            if (!Char.IsDigit(chr) && chr != 8)
            {
                e.Handled = true;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Solo permite letras
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Cliente_Load(object sender, EventArgs e)
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Cliente WHERE IdUsuario=" + idUsuario.ToString();
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                txtNombre.Text = dt.Rows[0]["Nombre"].ToString();
                txtTelefono.Text = dt.Rows[0]["Telefono"].ToString();
                txtDireccion.Text = dt.Rows[0]["Direccion"].ToString();
                txtEdad.Text = dt.Rows[0]["Edad"].ToString();
            }
        }
    }
}
