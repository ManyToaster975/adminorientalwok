﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Common;

namespace Prototipo
{
    public partial class Menu : Form
    {
        public Menu(Main m,int idUsuario)
        {
            InitializeComponent();
            this.m = m;
            this.idUsuario = idUsuario;
            this.CenterToScreen();
        }
        public static String cnn = "Provider=Microsoft.Jet.OLEDB.4.0 ;Data Source=" + Application.StartupPath + "\\BD.mdb";
        Main m;
        int idUsuario;
        int idCliente;
        

        
        private void btnOrdenar_Click(object sender, EventArgs e)
        {
            Ordenar ord = new Ordenar(this, idCliente);
            ord.Show();
            this.Hide();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            cargarMenu();
        }

    public void cargarMenu()
        {
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Cliente WHERE IdUsuario=" + idUsuario.ToString();
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                lblSaludo.Text = dt.Rows[0]["Nombre"].ToString();
                lblEdad.Text = dt.Rows[0]["Edad"].ToString();
                lblDireccion.Text = dt.Rows[0]["Direccion"].ToString();
                lblTelefono.Text = dt.Rows[0]["Telefono"].ToString();
                idCliente = Int32.Parse(dt.Rows[0]["IdCliente"].ToString());
                pnMenu.Visible = true;
                panel4.Visible = false;
            }
            else
            {
                Cliente cliente = new Cliente(idUsuario, this);
                panel3.Controls.Add(cliente);
                cliente.Location = new Point(100, 130);
                pnMenu.Visible = false;
                panel4.Visible = false;
            }
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("¿Desea cerrar sesión?", "Advertencia", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                m.Show();
                this.Close();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Cliente cliente = new Cliente(idUsuario, this);
            panel3.Controls.Add(cliente);
            cliente.Location = new Point(119, 119);
            pnMenu.Visible = false;
        }

        private void btnPast_Click(object sender, EventArgs e)
        {
            //Corrige
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            String str = "SELECT * FROM Pedidos WHERE IdCliente=" + idCliente.ToString();
            da = new OleDbDataAdapter(str, cnn);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                //Habilitar
                PedidosAnteriores pa = new PedidosAnteriores(this, idCliente);
                pa.Show();
                this.Hide();
            }
            else
                MessageBox.Show("No tiene ningún pedido");
        }

        private void pnMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_Cerrar_Click(object sender, EventArgs e)
        {
            DialogResult dg = MessageBox.Show("Desea salir del sistema?", "AVISO", MessageBoxButtons.YesNo);
            if (dg == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        
    }
}
