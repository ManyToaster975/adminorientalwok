﻿namespace Prototipo
{
    partial class Articulo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblArt = new System.Windows.Forms.Label();
            this.txtDes = new System.Windows.Forms.RichTextBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.pb = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            this.SuspendLayout();
            // 
            // lblArt
            // 
            this.lblArt.AutoSize = true;
            this.lblArt.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblArt.Location = new System.Drawing.Point(109, 3);
            this.lblArt.Name = "lblArt";
            this.lblArt.Size = new System.Drawing.Size(95, 25);
            this.lblArt.TabIndex = 10;
            this.lblArt.Text = "Nombre";
            // 
            // txtDes
            // 
            this.txtDes.BackColor = System.Drawing.SystemColors.Control;
            this.txtDes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDes.Enabled = false;
            this.txtDes.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Italic);
            this.txtDes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.txtDes.Location = new System.Drawing.Point(109, 30);
            this.txtDes.Name = "txtDes";
            this.txtDes.Size = new System.Drawing.Size(288, 73);
            this.txtDes.TabIndex = 23;
            this.txtDes.Text = "Descripcion";
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.Green;
            this.btn_add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_add.FlatAppearance.BorderSize = 0;
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("Century Gothic", 11F, System.Drawing.FontStyle.Bold);
            this.btn_add.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btn_add.Image = global::Prototipo.Properties.Resources.menu1;
            this.btn_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_add.Location = new System.Drawing.Point(3, 109);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(392, 34);
            this.btn_add.TabIndex = 24;
            this.btn_add.Text = "Agregar a carrito";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // pb
            // 
            this.pb.Location = new System.Drawing.Point(3, 3);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(100, 100);
            this.pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pb.TabIndex = 1;
            this.pb.TabStop = false;
            // 
            // Articulo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.pb);
            this.Controls.Add(this.txtDes);
            this.Controls.Add(this.lblArt);
            this.Name = "Articulo";
            this.Size = new System.Drawing.Size(400, 148);
            this.Load += new System.EventHandler(this.Artículo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.Label lblArt;
        private System.Windows.Forms.RichTextBox txtDes;
        private System.Windows.Forms.Button btn_add;
    }
}
