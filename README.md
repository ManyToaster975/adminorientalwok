# AdminOrientalWok

Proyecto realizado en el IDE Visual Studio 2012 hecho en lenguaje C# 

Consta de un sistema de administracion para restaurantes en el que el cliente
puede pedir algun producto o checar el status del mismo platillo que adquirio
ver informacion de otros restaurantes entre otras cosas.

Se maneja por una base de datos SQL Access alojada dentro del programa en la 
siguiente direccion: Prototipo\Prototipo\bin\Debug\BD.mdb

No se requiere instalacion externa solo algunos complementos como el Framework.net o alguno otro.

---
## Imagenes

![Imgur](https://i.imgur.com/fFTgrxi.png) ![Imgur](https://i.imgur.com/eZgrJRC.png)
![Imgur](https://i.imgur.com/7moRUzX.png) ![Imgur](https://i.imgur.com/CdNzGz5.png)
![Imgur](https://i.imgur.com/OXmOi0u.png) ![Imgur](https://i.imgur.com/BSyixCo.png)
